package day9;

import java.util.Scanner;
import java.math.BigDecimal;

public class LmsAxis {

	private static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		String answer = "Y";

		while (answer.toUpperCase().equals("Y")) {
			try {
				System.out.println("Enter the number of case: ");
				int number = input.nextInt();
				input.nextLine();

				switch (number) {
				case 1:
					soal1();
					break;
				case 2:
					soal2();
					break;
				case 3:
					soal3();
					break;
				case 4:
					soal4();
					break;
				case 5:
					soal5();
					break;
				case 6:
					soal6();
					break;
				case 7:
					soal7();
					break;
				case 8:
					soal8();
					break;
				default:
					System.out.println("Case is not available. Try again!");
				}

			} catch (Exception e) {
				System.out.println(e.getMessage());
			}

			System.out.println();
			System.out.print("Continue? ");
			answer = input.next();
		}
	}

	private static void soal1() {
		System.out.println("Masukkan nilai n: ");
		int n = input.nextInt();
		System.out.println("Masukkan array: ");
		String[] arrays = new String[n];
		for (int i = 0; i < arrays.length; i++) {
			arrays[i] = input.next();
		}

		BigDecimal[] bigArrays = new BigDecimal[arrays.length];
		// convert ke big decimal
		for (int i = 0; i < arrays.length; i++) {
			bigArrays[i] = new BigDecimal(arrays[i]);
		}

		// sorting dari kecil ke besar
		BigDecimal temp = new BigDecimal("0");
		for (int i = 0; i < bigArrays.length; i++) {// 3 2 4
			for (int j = i; j < bigArrays.length; j++) {// 3 2 4
				if (bigArrays[i].compareTo(bigArrays[j]) == 1 || bigArrays[i].compareTo(bigArrays[j]) == 0) {
					temp = bigArrays[i];
					bigArrays[i] = bigArrays[j];
					bigArrays[j] = temp;
				}
			}
		}

		// tampilkan
		for (int i = 0; i < bigArrays.length; i++) {
			System.out.println(bigArrays[i] + "");
		}

	}

	private static void soal2() {
		System.out.println("Masukkan panjang array: ");
		int panjangArray = input.nextInt();
		input.nextLine();
		System.out.println("Masukkan array: ");
		String inputan = input.nextLine();
		String[] arrayInputan = inputan.split(" ");

		if (panjangArray != arrayInputan.length) {
			System.out.println("input panjang array salah");
			return;
		}

		// convert ke int
		int[] arrayIntInputan = new int[arrayInputan.length];
		int[] arrayIntUnsorted = new int[arrayInputan.length];
		for (int i = 0; i < arrayInputan.length; i++) {
			arrayIntInputan[i] = Integer.parseInt(arrayInputan[i]);// sorted
			arrayIntUnsorted[i] = Integer.parseInt(arrayInputan[i]);// unsorted
		}

		// sorting dari terkecil ke terbesar
		int temp = 0;
		for (int i = 0; i < arrayIntInputan.length; i++) {
			for (int j = 0; j < arrayIntInputan.length; j++) {
				if (arrayIntInputan[i] <= arrayIntInputan[j]) {
					temp = arrayIntInputan[i];
					arrayIntInputan[i] = arrayIntInputan[j];
					arrayIntInputan[j] = temp;
				}
			}
		}

		// tampilan biar sama dengan lms
		for (int i = arrayIntUnsorted.length - 1; i > 0; i--) {
			arrayIntUnsorted[i] = arrayIntInputan[i];// 8 6 4 3 2

			for (int j = 0; j < arrayIntUnsorted.length; j++) {
				System.out.print(arrayIntUnsorted[j] + " ");
			}
			System.out.println();
		}
	}

	private static void soal3() {
		System.out.println("Masukkan panjang array: ");
		int panjangArray = input.nextInt();
		input.nextLine();
		System.out.println("Masukkan array: ");
		String inputan = input.nextLine();
		String[] arrayInputan = inputan.split(" ");

		if (panjangArray != arrayInputan.length) {
			System.out.println("input panjang array salah");
			return;
		}

		// convert ke int
		int[] arrayIntInputan = new int[panjangArray];
		for (int i = 0; i < panjangArray; i++) {
			arrayIntInputan[i] = Integer.parseInt(arrayInputan[i]);
		}

		// sorting
		int temp = 0;
		for (int i = 0; i < panjangArray; i++) {
			for (int j = 0; j < panjangArray; j++) {
				if (arrayIntInputan[i] <= arrayIntInputan[j]) {
					temp = arrayIntInputan[i];
					arrayIntInputan[i] = arrayIntInputan[j];
					arrayIntInputan[j] = temp;
				}
			}
		}

		// tampil
		for (int i = 0; i < panjangArray; i++) {
			System.out.print(arrayIntInputan[i] + " ");
		}
	}

	private static void soal4() {
		System.out.println("Masukkan panjang array: ");
		int panjangArray = input.nextInt();
		input.nextLine();
		System.out.println("Masukkan array: ");
		String inputan = input.nextLine();
		String[] arrayInputan = inputan.split(" ");
		// total shift = 4
		// 1 2 3 1 2 == 1
		// 1 1 2 3 2 == 2
		// 1 1 2 2 3 == 1

		if (panjangArray != arrayInputan.length) {
			System.out.println("input panjang array salah");
			return;
		}

		// convert ke int
		int[] arrayIntInputan = new int[arrayInputan.length];
		for (int i = 0; i < arrayInputan.length; i++) {
			arrayIntInputan[i] = Integer.parseInt(arrayInputan[i]);
		}

		// sorting dari terkecil ke terbesar
		int totalShift = 0;
		int temp = 0;
		for (int i = 0; i < arrayIntInputan.length; i++) {
			for (int j = i; j < arrayIntInputan.length; j++) {
				if (arrayIntInputan[i] > arrayIntInputan[j]) {
					temp = arrayIntInputan[i];
					arrayIntInputan[i] = arrayIntInputan[j];
					arrayIntInputan[j] = temp;
					totalShift++;
				}
			}
		}

		System.out.println(totalShift);
//		printArray(arrayIntInputan);

	}

	private static void printArray(int[] array) {
		String temp = "";
		for (int i = 0; i < array.length; i++) {
			if (i == array.length - 1) {
				temp += array[i];
			} else {
				temp += array[i] + " ";
			}
		}
		System.out.println(temp);
	}

	private static void soal5() {
		System.out.println("Masukkan panjang array: ");
		int panjangArray = input.nextInt();
		input.nextLine();
		System.out.println("Masukkan array: ");
		String inputan = input.nextLine();
		String[] arrayInputan = inputan.split(" ");

		if (panjangArray != arrayInputan.length) {
			System.out.println("input panjang array salah");
			return;
		}

		// convert ke int
		int[] arrayIntInputan = new int[arrayInputan.length];
		for (int i = 0; i < arrayInputan.length; i++) {
			arrayIntInputan[i] = Integer.parseInt(arrayInputan[i]);
		}

		// cari nilai min dan max
		int min = Integer.MAX_VALUE;
		int max = Integer.MIN_VALUE;
		for (int i = 0; i < panjangArray; i++) {
			if (arrayIntInputan[i] < min) {
				min = arrayIntInputan[i];
			}
			if (arrayIntInputan[i] > max) {
				max = arrayIntInputan[i];
			}
		}

		// buat array kosong berdasarkan min dan max
		int panjangArrayBaru = (max + 1) - (min - 1);// 4 - 0
		int[] arrayBaru = new int[panjangArrayBaru];

		// isi array kosong berdasarkan banyak data dari array inputan
		for (int i = 0; i < panjangArrayBaru; i++) {
			for (int j = 0; j < arrayIntInputan.length; j++) {
				if (i == arrayIntInputan[j]) {
					arrayBaru[i]++;
				}
			}
		}

		// tampilkan
		printArray(arrayBaru);

	}

	private static void soal6() {
		System.out.println("Masukkan panjang array: ");
		int panjangArray = input.nextInt();
		input.nextLine();
		System.out.println("Masukkan array: ");
		String inputan = input.nextLine();
		String[] arrayInputan = inputan.split(" ");

		if (panjangArray != arrayInputan.length) {
			System.out.println("input panjang array salah");
			return;
		}

		// convert ke int
		int[] arrayIntInputan = new int[arrayInputan.length];
		for (int i = 0; i < arrayInputan.length; i++) {
			arrayIntInputan[i] = Integer.parseInt(arrayInputan[i]);
		}

		// cari nilai min dan max
		int min = Integer.MAX_VALUE;
		int max = Integer.MIN_VALUE;
		for (int i = 0; i < panjangArray; i++) {
			if (arrayIntInputan[i] < min) {
				min = arrayIntInputan[i];
			}
			if (arrayIntInputan[i] > max) {
				max = arrayIntInputan[i];
			}
		}

		// buat array kosong berdasarkan min dan max
		int panjangArrayBaru = (max + 1) - (min - 1);// 4 - 0
		int[] arrayBaru = new int[panjangArrayBaru];

		// isi array kosong berdasarkan banyak data dari array inputan
		for (int i = 0; i < panjangArrayBaru; i++) {
			for (int j = 0; j < arrayIntInputan.length; j++) {
				if (i == arrayIntInputan[j]) {
					arrayBaru[i]++;
				}
			}
		}

		// tampilkan
		printArray(arrayBaru);
		String output = "";
		for (int i = 0; i < arrayBaru.length; i++) {
			for (int j = 0; j < arrayBaru[i]; j++) {
				output += i + " ";
			}
		}

		System.out.println(output);

	}

	private static void soal7() {
		System.out.println("Masukkan panjang array: ");
		int panjangArray = input.nextInt();
		input.nextLine();
		System.out.println("Masukkan array: ");
		String inputan = input.nextLine();
		String[] arrayInputan = inputan.split(" ");

		if (panjangArray != arrayInputan.length) {
			System.out.println("input panjang array salah");
			return;
		}

		// convert ke int
		int[] arrayIntInputan = new int[arrayInputan.length];
		for (int i = 0; i < arrayInputan.length; i++) {
			arrayIntInputan[i] = Integer.parseInt(arrayInputan[i]);
		}

		// sorting
		int temp = 0;
		for (int i = 0; i < arrayIntInputan.length; i++) {
			for (int j = i; j < arrayIntInputan.length; j++) {
				if (arrayIntInputan[i] > arrayIntInputan[j]) {
					temp = arrayIntInputan[i];
					arrayIntInputan[i] = arrayIntInputan[j];
					arrayIntInputan[j] = temp;
				}
			}
		}
		printArray(arrayIntInputan);

		int indeksMedian = (int) Math.ceil(panjangArray / 2);
		System.out.println("Median: " + arrayIntInputan[indeksMedian]);
	}

	private static void soal8() {
		System.out.print("Masukkan array karakter: ");
		String inputan = input.next();
		char[] arrayInputan = inputan.toCharArray();

		// sorting
		char temp = 0;
		for (int i = 0; i < arrayInputan.length; i++) {
			for (int j = i; j < arrayInputan.length; j++) {
				if (arrayInputan[i] > arrayInputan[j]) {
					temp = arrayInputan[i];
					arrayInputan[i] = arrayInputan[j];
					arrayInputan[j] = temp;
				}
			}
		}

		// tampil
		String output = "";
		for (int i = 0; i < arrayInputan.length; i++) {
			if (i == arrayInputan.length - 1) {
				output += arrayInputan[i] + "";
			} else {
				output += arrayInputan[i] + " ";
			}
		}
		System.out.println(output);
	}

}
