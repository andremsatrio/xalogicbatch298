package day9;

import java.util.Date;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Scanner;
import java.math.BigDecimal;

public class Main {

	private static Scanner input = new Scanner(System.in);

	public static void main(String[] args) throws ParseException {
		String answer = "Y";

		while (answer.toUpperCase().equals("Y")) {
			try {
				System.out.println("Enter the number of case: ");
				int number = input.nextInt();
				input.nextLine();

				switch (number) {
				case 1:
					soal1();
					break;
				case 2:
					soal2();
					break;
				case 3:
					soal3();
					break;
				case 4:
					soal4();
					break;
				case 5:
					soal5();
					break;
				case 6:
					soal6();
					break;
				case 7:
					soal7();
					break;
				case 8:
					soal8();
					break;
				default:
					System.out.println("Case is not available. Try again!");
				}

			} catch (Exception e) {
				System.out.println(e.getMessage());
			}

			System.out.println();
			System.out.print("Continue? ");
			answer = input.next();
		}

	}

	private static void soal1() {
		System.out.println("*** Judi Online Game ***");

		System.out.println("Point: ");
		int poin = input.nextInt();

		String mainLagi = "y";
		while (mainLagi.toLowerCase().equals("y")) {

			System.out.println("Taruhan: ");
			int taruhan = input.nextInt(); // tidak boleh bernilai minus dan lebih dari poin

			System.out.println("Tebak (U/D): ");
			char tebak = 0; // 68=D, 85 = U

			// jika inputan selain U dan D, maka iterasi ulang
			String tempTebak = input.next().toUpperCase();
			if (!tempTebak.equals("D") && !tempTebak.equals("U")) {
				System.out.println("Anda salah memasukkan karakter");
				continue;
			}

			// simpan inputan ke variable "tebak"
			tebak = tempTebak.charAt(0);

			// jika taruhan melebihi poin, maka iterasi ulang
			if (taruhan > poin) {
				System.out.println("poin anda kurang");
				continue;
			}

			// jika taruhan bernilai minus, maka iterasi ulang
			if (taruhan < 0) {
				System.out.println("taruhan anda kurang");
				continue;
			}
			String output = "";

			// kurangi poin dengan taruhan
			poin -= taruhan;

			// permainan dimulai
			int dadu = (int) (Math.random() * 9);
			output += dadu + "\n";

			if (dadu < 5) {
				// hasil D
				if (tebak == 68) { // hasil tebakan sama dengan dadu
					// win
					output += "You Win\n";
					poin += (taruhan * 2);
				} else {
					// lose
					output += "You Lose\n";
				}
			} else if (dadu > 5) {
				// hasil U
				if (tebak == 85) { // hasil tebakan sama dengan dadu
					// win
					output += "You Win\n";
					poin += (taruhan * 2);
				} else {
					// lose
					output += "You Lose\n";
				}
			} else {
				// SERI
				output += "SERI\n";
				poin += taruhan;
			}

			output += "Point saat ini: " + poin;
			System.out.println(output);
			System.out.println();

			if (poin <= 0) {
				System.out.println("Game Over");
				mainLagi = "n";
				continue;
			}

			System.out.println("Main lagi? (y/n)");
			mainLagi = input.next();
		}
	}

	private static void soal2() {
		System.out.println("*** Buah dan Keranjang Game ***");

		String[] keranjang = new String[3];
		int totalBuah = 0;
		String keranjangYangDibawaPegi = "";

		boolean isLoop = true;
		while (isLoop) {
			System.out.print("Masukkan keranjang 1 = ");
			keranjang[0] = input.next();
			System.out.print("Masukkan keranjang 2 = ");
			keranjang[1] = input.next();
			System.out.print("Masukkan keranjang 3 = ");
			keranjang[2] = input.next();

			System.out.print("Masukkan keranjang yang dibawa ke pasar (gunakan koma) = ");
			String[] keranjangPasar = input.next().toLowerCase().split(",");
			int[] intKeranjangPasar = new int[keranjangPasar.length];

			boolean isSalahInput = false;

			totalBuah = 0;

			int[] intKeranjang = new int[keranjang.length];
			for (int i = 0; i < keranjang.length; i++) {
				// periksa apakah inputan sama dengan "kosong"
				if (keranjang[i].toLowerCase().trim().equals("kosong") || keranjang[i].isEmpty()) {
					intKeranjang[i] = 0;
					continue;
				}

				// periksa apakah inputan selain angka
				char[] charBuah = keranjang[i].toCharArray();
				for (int j = 0; j < charBuah.length; j++) {
					if ((int) (charBuah[j]) >= 48 && (int) (charBuah[j]) <= 57) {
						isSalahInput = false;
					} else {
						isSalahInput = true;
						break;
					}
				}

				// simpan string array ke int array
				intKeranjang[i] += Integer.parseInt(keranjang[i]);
			}
			if (isSalahInput) {
				System.out.println("Anda salah memasukkan nilai keranjang");
				System.out.println("Keranjang 1: Kosong/angka/angka");
				continue;
			}

			// cek inputan keranjang yang dibawa kepasar
			for (int i = 0; i < keranjangPasar.length; i++) {
				// periksa apakah inputan selain angka
				char[] charKeranjangPasar = keranjangPasar[i].toCharArray();
				for (int j = 0; j < charKeranjangPasar.length; j++) {
					if ((int) (charKeranjangPasar[j]) >= 48 && (int) (charKeranjangPasar[j]) <= 57) {
						isSalahInput = false;
					} else if ((int) (charKeranjangPasar[j]) >= 44) {
						isSalahInput = false;
					} else {
						isSalahInput = true;
						break;
					}
				}

				// masukkan ke int
				intKeranjangPasar[i] = Integer.parseInt(keranjangPasar[i]);

				// masukkan ke string
				if (i < intKeranjangPasar.length - 1) {
					keranjangYangDibawaPegi += intKeranjangPasar[i] + ", ";
				} else {
					keranjangYangDibawaPegi += intKeranjangPasar[i];
				}

			}

			if (isSalahInput) {
				System.out.println("Anda salah memasukkan nilai keranjang yang dibawa kepasar");
				System.out.println("Keranjang 1: Kosong/angka/angka");
				continue;
			}

			int nilaiNol = 0;

			// hitung buah
			for (int i = 0; i < intKeranjang.length; i++) {
				boolean apaKeranjangDiTinggal = true;
				for (int j = 0; j < intKeranjangPasar.length; j++) {
					if (i == intKeranjangPasar[j] - 1) {
						apaKeranjangDiTinggal = false;
						break;
					}
				}
				if (apaKeranjangDiTinggal) {
					totalBuah += intKeranjang[i];
				}

				// periksa apakah keranjang yang bernilai 0 lebih dari 1
				if (intKeranjang[i] == 0) {
					nilaiNol++;
				}
				if (nilaiNol > 1) {
					System.out.println("Keranjang kosong terdeteksi lebih dari satu");
					isSalahInput = true;
					break;
				}
			}
			if (isSalahInput) {
				continue;
			}

			isLoop = false;
		}

		System.out.println("Keranjang " + keranjangYangDibawaPegi + " dibawa ke pasar");
		System.out.println();
		System.out.println("Sisa buah = " + totalBuah);

	}

	private static void soal3() {
		System.out.println("*** Orang Duduk Game ***");

		System.out.print("x = ");
		int orang = input.nextInt();
		int temp = 1;

		if (orang < 0) {
			System.out.println("MINUS? Anda yakin?");
			return;
		}

		// faktorial
		for (int i = 1; i <= orang; i++) {
			temp *= i;
		}

		System.out.println("Ada " + temp + " cara");

	}

	private static void soal4() {
		System.out.println("*** Pengungsian Game ***");

		// laki, perempuan, anak, bayi
		int[] orang = new int[4];
		System.out.print("Laki dewasa = ");
		orang[0] = input.nextInt() * 1;
		System.out.print("Wanita dewasa = ");
		orang[1] = input.nextInt() * 3;
		System.out.print("Anak-anak = ");
		orang[2] = input.nextInt() * 3;
		System.out.print("Bayi = ");
		orang[3] = input.nextInt() * 5;

		int baju = 0;

		for (int i = 0; i < orang.length; i++) {
			baju += orang[i];
		}
		if (baju % 2 != 0 || baju <= 10) {
			baju -= orang[1] / 3;
		}

		System.out.println(baju + " Baju");

	}

	private static void soal5() {
		System.out.println("*** Kue Pukis Game ***");

		System.out.print("Jumlah kue pukis : ");
		int totalKue = input.nextInt();

		if (totalKue < 1) {
			System.out.println("input yang anda masukkan salah");
			return;
		}

		// terigu,gula,susu,telur
		String[] namaBahan = { "Tepung terigu", "Gula Pasir", "Susu Murni", "Putih Telur Ayam" };
		float[] jumlahBahan = new float[namaBahan.length];

		for (int i = 0; i < jumlahBahan.length; i++) {
			System.out.print("Masukkan jumlah " + namaBahan[i] + " (gram): ");
			jumlahBahan[i] = (float) (input.nextInt() / (totalKue * 1.0));
		}
		System.out.println();

		DecimalFormat decimalFormat = new DecimalFormat("0.000");

		String output = "Jumlah bahan yang diperlukan untuk membuat 1 kue pukis adalah\n";
		for (int i = 0; i < namaBahan.length; i++) {
			output += "- " + namaBahan[i] + " : " + decimalFormat.format(jumlahBahan[i]) + " gram\n";
		}
		System.out.println(output);
	}

	private static void soal6() throws ParseException {
		System.out.println("*** Parkir Game ***");

		boolean isLoop = true;
		while (isLoop) {
			System.out.print("Masukkan waktu masuk (dd/MM/yyyy HH.mm): ");
			String waktuMasuk = input.nextLine();// "31/08/2019 07.00";//
			System.out.print("Masukkan waktu keluar (dd/MM/yyyy HH.mm): ");
			String waktuKeluar = input.nextLine();// "20/09/2019 09.10";//

			// periksa apakah inputan sesuai format dd/MM/yyyy HH.mm
			String[] waktu = new String[2];
			waktu[0] = waktuMasuk;
			waktu[1] = waktuKeluar;
			boolean adaKarakterLain = false;
			boolean apaPanjangKarakter = false;
			boolean apaTanggalSesuai = true;
			for (int i = 0; i < waktu.length; i++) {
				char[] charWaktu = waktu[i].toCharArray();
				for (int j = 0; j < charWaktu.length; j++) {

					// jika ada karakter lain selain angka / . spasi
					if ((int) charWaktu[j] >= 46 && (int) charWaktu[j] <= 57) {
						adaKarakterLain = false;
					} else if ((int) charWaktu[j] == 32) {
						adaKarakterLain = false;
					} else {
						adaKarakterLain = true;
						break;
					}
				}
				if (adaKarakterLain) {
					break;
				}

				// periksa apakah panjang inputan sesuai
				if (charWaktu.length != 16) {// dd/MM/yyyy HH.mm
					apaPanjangKarakter = true;
					break;
				}

				// periksa apakah tanggal sesuai
				int hari = Integer.parseInt(waktu[i].substring(0, 2));
				int bulan = Integer.parseInt(waktu[i].substring(3, 5));
				if (bulan <= 7) {
					if (bulan % 2 == 1 && hari <= 31) {
						apaTanggalSesuai = true;
					} else if (bulan % 2 == 0 && hari <= 30) {
						apaTanggalSesuai = true;
					} else if (bulan == 2 && hari <= 28) {
						apaTanggalSesuai = true;
					} else {
						apaTanggalSesuai = false;
						break;
					}
				} else if (bulan > 7 && bulan <= 12) {
					if (bulan % 2 == 0 && hari <= 31) {
						apaTanggalSesuai = true;
					} else if (bulan % 2 == 1 && hari <= 30) {
						apaTanggalSesuai = true;
					} else {
						apaTanggalSesuai = false;
						break;
					}
				}

			}
			if (adaKarakterLain || apaPanjangKarakter || !apaTanggalSesuai) {
				System.out.println("Anda salah input waktu");
				continue;
			}

			int biayaParkir = 3000;

			Date dateMasuk = new SimpleDateFormat("dd/MM/yyyy HH.mm").parse(waktuMasuk);
			Date dateKeluar = new SimpleDateFormat("dd/MM/yyyy HH.mm").parse(waktuKeluar);

			// jika waktu keluar kurang dari sama dengan waktu masuk
			if (dateKeluar.getTime() <= dateMasuk.getTime()) {
				System.out.println("Anda salah input waktu");
				continue;
			}

			// stop while loop
			isLoop = false;

			long millis = dateKeluar.getTime() - dateMasuk.getTime();

			double jam = (double) ((millis / 1000.0 / 3600.0));

			// jika durasi > 2:30 jam, bulatkan keatas jadi 3 jam
			if (jam > 2.5) { // 2.5
				jam = Math.ceil(jam); // 3.0
			} else {
				jam = (int) (jam); // 2.0
			}
			System.out.println();
			System.out.println("Total Jam: " + (int) (jam) + " jam");
			System.out.println("Total Biaya: Rp. " + new BigDecimal(biayaParkir * jam) + ",-");
		}

	}

	private static void soal7() throws ParseException {
		System.out.println("*** Pinjam Buku Game ***");

		boolean isLoop = true;
		while (isLoop) {
			System.out.print("Masukkan waktu pinjam (dd-MM-yyyy): ");
			String waktuPinjam = input.next();// "20-08-2019";//
			System.out.print("Masukkan waktu kembali (dd-MM-yyyy): ");
			String waktuKembali = input.next();// "24-08-2019";//

			// periksa apakah inputan sesuai format dd-MM-yyyy
			String[] waktu = new String[2];
			waktu[0] = waktuPinjam;
			waktu[1] = waktuKembali;
			boolean adaKarakterLain = false;
			boolean apaPanjangKarakter = false;
			boolean apaTanggalSesuai = true;
			for (int i = 0; i < waktu.length; i++) {
				char[] charWaktu = waktu[i].toCharArray();
				for (int j = 0; j < charWaktu.length; j++) {

					// jika ada karakter lain selain angka -
					if ((int) charWaktu[j] >= 48 && (int) charWaktu[j] <= 57) {
						adaKarakterLain = false;
					} else if ((int) charWaktu[j] == 45) {
						adaKarakterLain = false;
					} else {
						adaKarakterLain = true;
						break;
					}
				}
				if (adaKarakterLain) {
					break;
				}

				// periksa apakah panjang inputan sesuai
				if (charWaktu.length != 10) {
					apaPanjangKarakter = true;
					break;
				}

				// periksa apakah tanggal sesuai (tidak ada minus)
				String[] hariBulanTahun = waktu[i].split("-");
				if (hariBulanTahun.length > 3) {
					apaTanggalSesuai = false;
					break;
				}
				int hari = Integer.parseInt(hariBulanTahun[0]);
				int bulan = Integer.parseInt(hariBulanTahun[1]);
				int tahun = Integer.parseInt(hariBulanTahun[2]);
				boolean apaKabisat = tahunKabisat(tahun);
				if (bulan <= 7) {
					if (bulan % 2 == 1 && hari <= 31) {
						apaTanggalSesuai = true;
					} else if (bulan % 2 == 0 && hari <= 30) {
						apaTanggalSesuai = true;
					} else if (bulan == 2 && hari <= 28) {
						apaTanggalSesuai = true;
					} else if (bulan == 2 && hari <= 29 && apaKabisat) {
						apaTanggalSesuai = true;
					} else {
						apaTanggalSesuai = false;
						break;
					}
				} else if (bulan > 7 && bulan <= 12) {
					if (bulan % 2 == 0 && hari <= 31) {
						apaTanggalSesuai = true;
					} else if (bulan % 2 == 1 && hari <= 30) {
						apaTanggalSesuai = true;
					} else {
						apaTanggalSesuai = false;
						break;
					}
				}

			}
			if (adaKarakterLain || apaPanjangKarakter || !apaTanggalSesuai) {
				System.out.println("Anda salah input waktu");
				continue;
			}

			int denda = 500;

			Date datePinjam = new SimpleDateFormat("dd-MM-yyyy").parse(waktuPinjam);
			Date dateKembali = new SimpleDateFormat("dd-MM-yyyy").parse(waktuKembali);

			// jika waktu keluar kurang dari sama dengan waktu masuk
			if (dateKembali.getTime() <= datePinjam.getTime()) {
				System.out.println("Anda salah input waktu");
				continue;
			}

			// stop loop
			isLoop = false;

			long millis = dateKembali.getTime() - datePinjam.getTime();

			int hari = (int) (millis / 1000 / 3600 / 24);

			int bayarDenda = 0;

			if (hari > 3) {
				bayarDenda = (hari - 3) * denda;
			}

			String output = "";

			output += "Total hari: " + (int) hari + "\n";
			output += "Denda: Rp. " + new BigDecimal(bayarDenda) + ",-";

			System.out.println(output);
		}
	}

	private static boolean tahunKabisat(int tahun) {
		boolean apaBenar = false;
		if (tahun % 4 == 0) {
			if (tahun % 100 == 0) {
				if (tahun % 400 == 0) {
					apaBenar = true;
				} else {
					apaBenar = false;
				}
			} else {
				apaBenar = true;
			}
		} else {
			apaBenar = false;
		}

		return apaBenar;
	}

	private static void soal8() {
		System.out.println("*** Bakso Dibalik Game ***");

		System.out.print("Masukkan kata: ");
		String stringInput = input.next().toLowerCase();
		char[] charStringInput = stringInput.toCharArray();

		if (stringInput.equals("bakso") || stringInput.equals("sop")) {
			System.out.println("Tumpah");
			return;
		}

		String temp = "";
		for (int i = charStringInput.length - 1; i >= 0; i--) {
			if ((int) (charStringInput[i]) < 97 || (int) (charStringInput[i]) > 122) {
				System.out.println("Maaf inputan anda salah");
				return;
			}
			temp += charStringInput[i];
		}
		if (temp.equals(stringInput)) {
			System.out.println("Yes");
		} else {
			System.out.println("No");
		}

	}

}
