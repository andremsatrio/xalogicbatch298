package day5;

import java.util.Scanner;

public class Main {

	private static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {

		String answer = "Y";

		while (answer.toUpperCase().equals("Y")) {
			try {
				System.out.print("Enter the number of case: ");
				int number = input.nextInt();
				input.nextLine();

				switch (number) {
				case 1:
					soal1();
					break;
				case 2:
					soal2();
					break;
				case 3:
					soal3();
					break;
				case 4:
					soal4();
					break;
				case 5:
					soal5();
					break;
				case 6:
					soal6();
					break;
				default:
					System.out.println("Case is not available. Try again!!");
					break;

				}
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}

			System.out.println();
			System.out.print("Continue? ");
			answer = input.next();
		}

	}

	private static void soal1() {
		System.out.println("Soal 1 ------");

		System.out.print("Masukkan Uang Andi: ");
		int uangAndi = input.nextInt();
		input.nextLine();
		System.out.print("Masukkan Harga Baju: ");
		String hargaBaju = input.nextLine();
		System.out.print("Masukkan Harga Celana: ");
		String hargaCelana = input.nextLine();

		int temp1 = 0;
		int temp2 = 0;

		System.out.println(uangAndi + " " + hargaBaju + " " + hargaCelana);

		String[] splitBaju = hargaBaju.split(",");
		String[] splitCelana = hargaCelana.split(",");
		for (int i = 0; i < splitBaju.length; i++) {
			for (int j = 0; j < splitCelana.length; j++) {
				if (i == j) {
					temp1 = Integer.parseInt(splitBaju[i]) + Integer.parseInt(splitCelana[j]);
					if (temp1 <= uangAndi && temp1 >= temp2) {
						temp2 = temp1;
					}
				}
			}
		}

		System.out.println(temp2);
	}

	private static void soal2() {
		System.out.println("Soal 2 ------");

		System.out.print("Masukkan Array: ");
		String arr = input.nextLine();
		System.out.print("Masukkan nilai rotasi: ");
		int rot = input.nextInt();

		String temp = "";

		String[] arrayArr = arr.split(",");
		for (int i = 0; i < rot; i++) {
			for (int j = 0; j < arrayArr.length; j++) {
				if (j == arrayArr.length - 1) {
					temp = temp + arrayArr[0];
				} else {
					temp = temp + arrayArr[j + 1] + ",";
				}
			}
			System.out.print((i + 1) + ": ");
			System.out.println(temp);

			arrayArr = temp.split(",");
			temp = "";
		}
	}

	private static void soal3() {
		System.out.println("Soal 3 ------");

		System.out.print("Masukkan total puntung yang dikumpulkan: ");
		int puntungDiKumpul = input.nextInt();
		int satuBatang = 8;
		int sisa = puntungDiKumpul % satuBatang;

		int totalBatang = (int) (puntungDiKumpul / satuBatang);

		System.out.println("1. Total Batang rokok: " + totalBatang + " sisa puntung: " + sisa);

		System.out.println("2. Jumlah batang rokok yang mampu dirangkai: " + totalBatang + ", \ntotal penjualan: "
				+ totalBatang + " x Rp. 500,- = " + "Rp. " + (totalBatang * 500) + ",-");

	}

	private static void soal4() {
		System.out.println("Soal 4 ------");

		System.out.print("Masukkan total menu: ");
		int totalMenu = input.nextInt();
		System.out.print("Masukkan index alergi: ");
		int indexAlergi = input.nextInt();
		input.nextLine();
		System.out.print("Masukkan daftar harga menu: ");
		String hargaMenu = input.nextLine();
		System.out.print("Masukkan uang Elsa: ");
		int uangElsa = input.nextInt();

		int sisa = 0;

		int temp = 0;

		String[] arrayHargaMenu = hargaMenu.split(",");

		if (totalMenu != arrayHargaMenu.length) {
			System.out.println("Total menu dan total harga menu tidak sesuai");
			return;
		}

		for (int i = 0; i < arrayHargaMenu.length; i++) {
			if (i != indexAlergi) {
				temp += Integer.parseInt(arrayHargaMenu[i]);
			}
		}
		temp /= 2;
		System.out.println("Elsa harus membayar: Rp." + temp + ",-");

		sisa = uangElsa - temp;
		if (sisa < 0) {
			System.out.println("Elsa mengutang dimas: " + Math.abs(sisa));
		} else if (sisa == 0) {
			System.out.println("Elsa tidak memiliki sisa uang");
		} else {
			System.out.println("Sisa uang Elsa: Rp. " + sisa + ",-");
		}
	}

	private static void soal5() {
		System.out.println("Soal 5 ------");

		System.out.print("Masukkan kalimat yang anda mau: ");
		String vokalKonsonan = input.nextLine();
		String vokal = "";
		String konsonan = "";
		char charTemp = 0;

		// memisahkan vokal dan konsonan
		char[] charVokalKonsonan = vokalKonsonan.toLowerCase().toCharArray();
		for (int i = 0; i < charVokalKonsonan.length; i++) {
			charTemp = charVokalKonsonan[i];
			if (charTemp == 'a' || charTemp == 'i' || charTemp == 'u' || charTemp == 'e' || charTemp == 'o') {
				vokal += charTemp;
			} else {
				if (charTemp != ' ') {
					konsonan += charTemp;
				}
			}
		}

		char[] arrayVokal = vokal.toCharArray();
		char[] arrayKonsonan = konsonan.toCharArray();

		// mengurutkan vokal dari yang terkecil
		char temp1 = 0;// 1
		for (int i = 0; i < arrayVokal.length; i++) {
			for (int j = 0; j < arrayVokal.length; j++) {
				if ((int) arrayVokal[i] <= (int) arrayVokal[j]) {
					temp1 = arrayVokal[i];
					arrayVokal[i] = arrayVokal[j];
					arrayVokal[j] = temp1;
				}
			}
		}
		System.out.println("Vokal = " + String.valueOf(arrayVokal));

		// mengurutkan konsonan dari yang terkecil
		temp1 = 0;
		for (int i = 0; i < arrayKonsonan.length; i++) {
			for (int j = 0; j < arrayKonsonan.length; j++) {
				if ((int) arrayKonsonan[i] <= (int) arrayKonsonan[j]) {
					temp1 = arrayKonsonan[i];
					arrayKonsonan[i] = arrayKonsonan[j];
					arrayKonsonan[j] = temp1;
				}
			}
		}
		System.out.println("Konsonan = " + String.valueOf(arrayKonsonan));

	}

	private static void soal6() {
		System.out.println("Soal 6 ------");

		System.out.print("Masukkan kalimat: ");
		String kalimat = input.nextLine();
		String[] arrayKalimat = kalimat.split(" ");
		String temp = "";
		char charTemp = 0;

		for (int i = 0; i < arrayKalimat.length; i++) {
			char[] charArrayKalimat = arrayKalimat[i].toCharArray();
			for (int j = 0; j < charArrayKalimat.length; j++) {
				charTemp = charArrayKalimat[j];
				if (j % 2 == 1) {
					temp += "*";
				} else {
					temp += String.valueOf(charTemp);
				}
			}
			if (i == arrayKalimat.length - 1) {
				temp += "";
			} else {
				temp += " ";
			}
		}
		System.out.println(temp);
	}
}
