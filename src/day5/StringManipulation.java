package day5;

public class StringManipulation {

	public static void main(String[] args) {
		// String manipulation
		String words1 = "Aku Sayang Kamu";

		String[] tempWords1 = words1.split(" ");
		System.out.println(tempWords1[tempWords1.length - 1] + " Panjang array " + tempWords1.length);

		char[] charWords1 = tempWords1[1].toCharArray();
		System.out.println(charWords1[charWords1.length - 1]);

		String replaceWords1 = words1.replaceAll(" ", "");
		System.out.println(replaceWords1);

		char[] charReplaceWords1 = replaceWords1.toCharArray();
		System.out.println(charReplaceWords1[8]);

		String kata2 = words1.substring(4, 10);
		System.out.println(kata2);

		String lowerWords1 = words1.toLowerCase().replaceAll(" ", "");
		String tempKata2 = lowerWords1.substring(3, 9);
		System.out.println(tempKata2);

		char[] tempLowerWords1 = lowerWords1.toCharArray();
		System.out.println(tempLowerWords1);

		String upperWords1 = words1.toUpperCase();
		System.out.println(upperWords1);
	}

}
