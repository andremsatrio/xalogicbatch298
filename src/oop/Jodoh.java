package oop;

public class Jodoh {
	public int kalkulasiKecocokkan(int beban1, int beban2) {
		// perhitungan berdasarkan algoritma cinta
		// 1+1 = segalanya, 2-1 = null
		// jika genap maka cocok, jika ganjil maka tidak cocok

		int kecocokkan = 0;

		// cek apakah bernilai genap
		if ((beban1 + beban2) % 2 == 0) {
			kecocokkan += 50;
		} else {
			kecocokkan += 25;
		}
		// cek apakah bernilai ganjil
		if ((beban1) % 2 == 0) {
			kecocokkan += 25;
		} else {
			kecocokkan += 10;
		}
		// cek apakah bernilai ganjil
		if ((beban2) % 2 == 0) {
			kecocokkan += 25;
		} else {
			kecocokkan += 10;
		}
		return kecocokkan;
	}
}
