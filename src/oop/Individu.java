package oop;

public class Individu {
	String nama = "";
	char[] charNama = new char[0];
	int beban = 0;
	boolean adaKarakterLain = false;

	public void namaEjaan() {
		String namaTanpaSpasi = this.nama.toLowerCase().replaceAll(" ", "");
		this.charNama = namaTanpaSpasi.toCharArray();
	}

	public void bebanNama(char[] nama) {
		int beban = 0;
		for (int i = 0; i < nama.length; i++) {
			if ((nama[i] < 97 || nama[i] > 122) && nama[i] != 32) {
				this.adaKarakterLain = true;
				break;
			} else if (nama[i] != 32) {
				beban += ((int) (nama[i]) - 96);
			}
		}
		this.beban = beban;
	}

}