package oop;

import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class Main {

	private static Scanner input = new Scanner(System.in);

	public static void main(String[] args) throws InterruptedException {
		// animasi pembukaan
		pembukaan(true);

		String temp = "Tingkat kecocokkan anda dengan pasangan anda adalah sebesar: ";
		int kecocokkan = 0;

		// iniliasisasi individu
		Individu orang1 = new Individu();
		Individu orang2 = new Individu();

		System.out.println("Masukkan nama anda: ");
		orang1.nama = input.nextLine();
		System.out.println("Masukkan nama pasangan anda: ");
		orang2.nama = input.nextLine();

		// konvert dari string ke array
		orang1.namaEjaan();
		orang2.namaEjaan();

		// handling jika ada karakter lain
		if (orang1.adaKarakterLain || orang2.adaKarakterLain) {
			temp = "Maaf ada karakter lain selain huruf a-z";
			System.out.println(temp);
			return;
		}

		// jumlahkan karakter berdasarkan bebannya
		orang1.bebanNama(orang1.charNama);
		orang2.bebanNama(orang2.charNama);

		// masukkan atribut tiap individu ke temp
		temp = "Beban 1: " + orang1.beban + "\nBeban 2: " + orang2.beban + "\n" + temp;
		temp = "Char 1: " + String.valueOf(orang1.charNama) + "\nChar 2: " + String.valueOf(orang2.charNama) + "\n"
				+ temp;

		// kalkulasi di kelas jodoh
		Jodoh jodoh = new Jodoh();
		kecocokkan = jodoh.kalkulasiKecocokkan(orang1.beban, orang2.beban);

		// tampilkan hasilnya
		temp += kecocokkan + "%";
		System.out.println(temp);

//		pembukaan(false);
	}

	public static void pembukaan(boolean apaPembukaan) throws InterruptedException {
		String salam = "LOPE LOPE";
		salam = "\n===================================================\n" + salam;
		if (apaPembukaan) {
			salam += "\nSelamat datang di aplikasi (HOAX) kalkulasi perjodohan berdasarkan nama lengkap\n";
			salam += "LOPE LOPE";
			salam += "\n===================================================\n";
		} else {
			System.out.println();
			salam += "\nSaya harap Anda sadar apa yang Anda lakukan\n";
			salam += "LOPE LOPE";
			salam += "\n===================================================\n";
		}

		char[] charSalam = salam.toCharArray();
		for (int i = 0; i < charSalam.length; i++) {
			if (charSalam[i] == 32) {
				TimeUnit.MILLISECONDS.sleep(100);
			} else if (charSalam[i] != '=') {
				TimeUnit.MILLISECONDS.sleep(20);
			}
			System.out.print(charSalam[i]);
		}
//		TimeUnit.MILLISECONDS.sleep(100);
		System.out.println();
	}
}
