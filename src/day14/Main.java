package day14;

import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Scanner;

public class Main {

	private static Scanner input = new Scanner(System.in);

	public static void main(String[] args) throws ParseException {
		String answer = "Y";

		while (answer.toUpperCase().equals("Y")) {
			try {
				System.out.println("Enter the number of case: ");
				int number = input.nextInt();
				input.nextLine();

				switch (number) {
				case 1:
					soal1();
					break;
				case 2:
					soal2();
					break;
				default:
					System.out.println("Case is not available. Try again!");
				}

			} catch (Exception e) {
				System.out.println(e.getMessage());
			}

			System.out.println();
			System.out.print("Continue? ");
			answer = input.next();
		}

	}

	private static void soal1() throws ParseException {
		// Mary mendapat libur setiap x hari || kerja = 3 hari, libur = 1 hari
		// Susan mendapat libur setiap 2 hari || kerja = 2 hari, libur = 1 hari
		// Jika mereka terakhir mendapat libur di hari yang sama pada tanggal z
		// kapan tanggal terdekat mereka libur bersama
		// input: x=3,y=2,z=25 Februari 2020
		// output: 8 Maret 2020

		System.out.println("Masukkan libur Mary setiap x hari: ");
		int x = input.nextInt() + 1;// 4;//
		System.out.println("Masukkan libur Susan setiap y hari: ");
		int y = input.nextInt() + 1;// 3;// input.nextInt() + 1;
		input.nextLine();
		System.out.println("Masukkan tanggal terakhir libur bersama: ");
		String z = input.nextLine(); // input.nextLine(); // "25 February 2020";

		String output = "";

		// handling
		boolean apaInputanSesuai = true;
		boolean apaAdaKarakterLain = false;
		char[] waktu = z.toCharArray();
		for (int i = 0; i < waktu.length; i++) {
			// periksa apa ada karakter lain selain cth: 25 February 2020
			if ((waktu[i] >= 48 && waktu[i] <= 57) || waktu[i] == 32 || (waktu[i] >= 65 && waktu[i] <= 90)
					|| (waktu[i] >= 97 && waktu[i] <= 122)) {
				apaAdaKarakterLain = false;
			} else {
				apaAdaKarakterLain = true;
				break;
			}

			// periksa apa inputan sesuai
			if (i <= 1) {
				if (waktu[i] >= 48 && waktu[i] <= 57) {
					apaInputanSesuai = true;
				} else {
					apaInputanSesuai = false;
					break;
				}
			} else if (i > waktu.length - 5) {
				if ((waktu[i] >= 48 && waktu[i] <= 57) || waktu[i] == 32) {
					apaInputanSesuai = true;
				} else {
					apaInputanSesuai = false;
					break;
				}

				output += String.valueOf(waktu[i]);
			}
		}

		if (apaAdaKarakterLain || !apaInputanSesuai) {
			System.out.println("input tidak sesuai");
			return;
		}

		// ubah bulan dari bahasa indo ke eng
		String[] zSplit = z.split(" ");
		boolean apaNamaBulanSesuai = true;
		z = "";
		int intBulan = 0;
		for (int i = 0; i < zSplit.length; i++) {
			if (i == 1) {
				String tempBulan = zSplit[i].toLowerCase();
				String[] namaBulanIndo = "januari,februari,maret,april,mei,juni,juli,agustus,september,oktober,november,desember".split(",");
				String[] namaBulanEng = "january,february,march,april,may,june,july,august,september,october,november,december".split(",");
				for(int j=0;j<namaBulanIndo.length;j++) {
					if(tempBulan.equals(namaBulanIndo[j])) {
						tempBulan = namaBulanEng[j];
						intBulan = i+1;
						apaNamaBulanSesuai = true;
						break;
					} else {
						apaNamaBulanSesuai = false;
					}
				}
				z += " " + tempBulan + " ";
			} else {
				z += zSplit[i];
			}
		}
		if(!apaNamaBulanSesuai) {
			System.out.println("Nama Bulan tidak sesuai");
			return;
		}

		// handling kabisat
		zSplit = z.split(" ");
		boolean apaTanggalSesuai = true;
		int hari = Integer.parseInt(zSplit[0]);
		int bulan = intBulan;
		int tahun = Integer.parseInt(zSplit[0]);
		if (bulan > 0 && bulan <= 7) {
			if (bulan == 2 && hari <= 28) {
				apaTanggalSesuai = true;
			} else if (bulan == 2 && hari <= 29 && tahunKabisat(tahun)) {
				apaTanggalSesuai = true;
			} else if (bulan % 2 == 1 && hari <= 31) {
				apaTanggalSesuai = true;
			} else if (bulan % 2 == 0 && hari <= 30 && bulan != 2) {
				apaTanggalSesuai = true;
			} else {
				apaTanggalSesuai = false;
			}
		} else if (bulan > 7 && bulan <= 12) {
			if (bulan % 2 == 0 && hari <= 31) {
				apaTanggalSesuai = true;
			} else if (bulan % 2 == 1 && hari <= 30) {
				apaTanggalSesuai = true;
			} else {
				apaTanggalSesuai = false;
			}
		}

//		System.out.println(apaTanggalSesuai + " " + bulan + " " + tahunKabisat(tahun));
		if (!apaTanggalSesuai) {
			System.out.println("Tanggal tidak sesuai, silahkan periksa kembali");
			return;
		}

		SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMMM yyyy");
		Date dateZ = dateFormat.parse(z);
		System.out.println(dateZ);

		long hariLiburBersama = 0;
		boolean apaLiburBersama = false;
		while (!apaLiburBersama) {
			if (hariLiburBersama % x == 0 && hariLiburBersama % y == 0 && hariLiburBersama > 0) {
				apaLiburBersama = true;
				continue;
			}
			hariLiburBersama++;
		}

		output = dateFormat.format(dateZ.getTime() + (hariLiburBersama * 1000 * 3600 * 24));

		System.out.println(output);

	}

	private static boolean tahunKabisat(int tahun) {
		boolean apaBenar = false;
		if (tahun % 4 == 0) {
			if (tahun % 100 == 0) {
				if (tahun % 400 == 0) {
					apaBenar = true;
				} else {
					apaBenar = false;
				}
			} else {
				apaBenar = true;
			}
		} else {
			apaBenar = false;
		}

		return apaBenar;
	}

	private static void soal2() {
		// input: Sample Case
		// output: aa - c - ee - l - m - p - ss
		// Next Case

		System.out.println("Masukkan kalimat: ");
		char[] inputChar = input.nextLine().toLowerCase().trim().toCharArray();
		String output = "";

		// sorting
		char tempChar = 0;
		for (int i = 0; i < inputChar.length; i++) {
			for (int j = 0; j < inputChar.length; j++) {
				if (inputChar[i] < inputChar[j]) {
					tempChar = inputChar[i];
					inputChar[i] = inputChar[j];
					inputChar[j] = tempChar;
				}
			}
		}
		System.out.println(inputChar);

		tempChar = 0;
		for (int i = 0; i < inputChar.length; i++) {
			if(inputChar[i] != 32) {
				if (tempChar != inputChar[i] && i > 1) {
					output += " - ";
				}
				tempChar = inputChar[i];
				output += inputChar[i];
			}

		}

		System.out.println(output);
	}

}
