package tryout;

import java.math.BigInteger;
import java.util.Scanner;

public class Main {

	private static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		String answer = "Y";
		while (answer.toUpperCase().equals("Y")) {
			try {
				System.out.println("Enter the number of case: ");
				int num = input.nextInt();
				input.nextLine();

				switch (num) {
				case 1:
					soal1();
					break;
				case 2:
					soal2();
					break;
				case 3:
					soal3();
					break;
				case 4:
					soal4();
					break;
				case 5:
					soal5();
					break;
				case 6:
					soal6();
					break;
				case 7:
					soal7();
					break;
				case 8:
					soal8();
					break;
				case 9:
					soal9();
					break;
				case 10:
					soal10();
					break;
				default:
					System.out.println("Case is not available");
					break;
				}

			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
			System.out.println();
			System.out.print("Continue?");
			answer = input.next();

		}

	}

	private static void soal1() {
		System.out.println("Masukkan bilangan bulat positif: ");
		int n = input.nextInt();

		if (n < 0) {
			System.out.println("bilangan positif woyyy!!");
			return;
		}

		String tempGenap = "";
		String tempGanjil = "";
		for (int i = 1; i <= n; i++) {
			if (i % 2 == 0) {
				tempGenap += i;
			} else {
				tempGanjil += i;
			}
		}
		System.out.println(tempGanjil);
		System.out.println(tempGenap);
	}

	private static void soal2() {
		System.out.println("Masukkan kalimat: ");
		String n = input.nextLine().toLowerCase();
		char[] charN = n.replaceAll(" ","").toCharArray();

		// sorting
		char tempChar = 0;
		for (int i = 0; i < charN.length; i++) {
			for (int j = 0; j < charN.length; j++) {
				if (charN[i] < charN[j]) {
					tempChar = charN[i];
					charN[i] = charN[j];
					charN[j] = tempChar;
				}
			}
		}

		// pisah vokal konsonan
		String tempVokal = "";
		String tempKonsonan = "";
		for (int i = 0; i < charN.length; i++) {
			char value = charN[i];
			if (value == 'a' || value == 'i' || value == 'u' || value == 'e' || value == 'o') {
				tempVokal += value;
			} else if (value != ' ') {
				tempKonsonan += value;
			}
		}

		System.out.println(tempVokal);
		System.out.println(tempKonsonan);
	}

	private static void soal3() {
		System.out.println("Masukkan nilai n: ");
		int n = input.nextInt();
		if (n < 1 || n > 100) {
			System.out.println("n tidak sesuai");
			return;
		}

		String output = "";

		int temp = 3;
		for (int i = 0; i < n; i++) {
			if (i == 0) {
				output += (100) + " adalah \"Si Angka 1\"";
			} else {
				output += (100 + temp) + " adalah \"Si Angka 1\"";
			}
			if (i > 0) {
				temp *= 3;
			}

			if (i < n - 1) {
				output += "\n";
			}
		}

		System.out.println(output);

	}

	private static void soal4() {
		int[] jarak = { 2000, 500, 1500, 2500 };
		// 1 liter untuk 2500

		System.out.println("masukkan rute: ");
		String rute = input.nextLine().toLowerCase();// "Toko - Tempat 1 - Tempat 2 - Toko".toLowerCase();
		String[] arrayRute = rute.split(" - ");

		int tempTotalJarak = 0;
		for (int i = 0; i < arrayRute.length; i++) {
			if (i > 0) {
				String temp1 = arrayRute[i - 1];
				String temp2 = arrayRute[i];
				if (temp1.equals("toko") && temp2.equals("tempat 1")) {
					tempTotalJarak += jarak[0];
				} else if (temp1.equals("tempat 1") && temp2.equals("tempat 2")) {
					tempTotalJarak += jarak[1];
				} else if (temp1.equals("tempat 2") && temp2.equals("tempat 3")) {
					tempTotalJarak += jarak[2];
				} else if (temp1.equals("tempat 3") && temp2.equals("tempat 4")) {
					tempTotalJarak += jarak[3];
				}
			}
		}

		int bensin = tempTotalJarak * 2 / 2500;

		System.out.println(bensin);
	}

	private static void soal5() {
		System.out.println("Laki-laki dewasa: ");
		int laki = input.nextInt();
		System.out.println("perempuan dewasa: ");
		int perempuan = input.nextInt();
		System.out.println("Remaja: ");
		int remaja = input.nextInt();
		System.out.println("Anak-anak: ");
		int anak = input.nextInt();
		System.out.println("Balita");
		int balita = input.nextInt();

		int totalOrang = (laki) + (perempuan) + (remaja) + (anak) + (balita);
		int totalPorsi = (laki * 2) + (perempuan) + (remaja) + (anak) + (balita);

		if (totalOrang % 2 == 1 && totalOrang > 5) {
			totalPorsi += perempuan;
		}
		System.out.println(totalPorsi);

	}

	private static void soal6() {
		System.out.println("Masukkan pin: ");
		int pin = input.nextInt();
		System.out.println("Uang yang disetor: ");
		BigInteger uangDiSetor = new BigInteger(input.nextInt()+"");

		System.out.println("Pilihan transfer: 1. Antar Rekening 2. Antar Bank");
		int pilihTransfer = 0;
		boolean isLoop = true;
		while (isLoop) {
			System.out.print("Masukkan angka 1 atau 2:");
			pilihTransfer = input.nextInt();
//			System.out.println(pilihTransfer);
			if (pilihTransfer == 1 || pilihTransfer == 2) {
				isLoop = false;
			}
		}

		int rekeningTujuan = 0;
		int kodeBank = 0;
		BigInteger nominalTransfer = new BigInteger("0");
		if (pilihTransfer == 1) { // Antar rekening
			System.out.println("Masukkan rekening tujuan: ");
			rekeningTujuan = input.nextInt();
			System.out.println("Masukkan nominal transfer: ");
			nominalTransfer = new BigInteger(input.nextInt()+"");
		} else { // Antar Bank
			System.out.println("masukkan kode bank: ");
			kodeBank = input.nextInt();
			System.out.println("Masukkan rekekning tujuan: ");
			rekeningTujuan = input.nextInt();
			System.out.println("Masukkan nominal transfer: ");
			nominalTransfer = new BigInteger(input.nextInt()+"");
		}

		if (nominalTransfer.compareTo(uangDiSetor) == 1) {
			System.out.println("Transaksi tidak berhasil, saldo anda saat ini Rp. " + uangDiSetor + ",-");
			return;
		}
		uangDiSetor = uangDiSetor.subtract(nominalTransfer);
		System.out.println("Tansaksi berhasil, saldo anda saat ini Rp. " + uangDiSetor + ",-");

	}

	private static void soal7() {
		System.out.println("Masukkan jumlah kartu: ");
		int jumlahKartu = input.nextInt();

		boolean gameOver = false;
		boolean surrender = false;
		while (!gameOver && !surrender) {
			System.out.println("Masukkan jumlah tawaran:");
			int jumlahTawaran = input.nextInt();

			if (jumlahTawaran > jumlahKartu) {
				System.out.println("tawaran melebihi batas");
				continue;
			}

			System.out.println("Pilih kotak 1 atau 2: ");
			boolean apaPilihKotakA = true;
			boolean isLoop = true;
			while (isLoop) {
				System.out.println("masukkan 1 atau 2:");
				int intKotak = input.nextInt();
				if (intKotak == 1) {
					apaPilihKotakA = true;
					isLoop = false;
				} else if (intKotak == 2) {
					apaPilihKotakA = false;
					isLoop = false;
				}
			}

			int[] kotakAB = new int[2];
			boolean apaMenang = false;
			boolean apaSeri = false;
			kotakAB[0] = (int) (Math.random() * 9);
			kotakAB[1] = (int) (Math.random() * 9);

			if (kotakAB[0] > kotakAB[1]) {
				if (apaPilihKotakA) {
					jumlahKartu += jumlahTawaran;
					apaMenang = true;
				} else {
					jumlahKartu -= jumlahTawaran;
					apaMenang = false;
				}
			} else if (kotakAB[1] < kotakAB[1]) {
				if (apaPilihKotakA) {
					jumlahKartu -= jumlahTawaran;
					apaMenang = false;
				} else {
					jumlahKartu += jumlahTawaran;
					apaMenang = true;
				}
			} else {
				apaSeri = true;
			}

			String menang = "";
			if (apaMenang) {
				menang = "You Win";
			} else {
				menang = "You Lose";
			}
			if (apaSeri) {
				menang = "seri";
			}

			System.out.println("Angka kotak A:" + kotakAB[0]);
			System.out.println("Angka kotak B:" + kotakAB[1]);
			System.out.println(menang);

			System.out.println("Anda menyerah???");
			input.nextLine();
			String answer = input.nextLine();
			if (answer.toLowerCase().equals("y")) {
				surrender = true;
			}

			if (jumlahKartu <= 0) {
				gameOver = true;
			}
		}
	}

	private static void soal8() {
		System.out.println("Input panjang deret: ");
		int panjangDeret = input.nextInt();

		String ganjil = "";
		String genap = "";
		boolean isLoop = true;
		int n = 0;
		while (isLoop) {
			if (ganjil.split(" ").length >= panjangDeret || genap.split(" ").length >= panjangDeret) {
				isLoop = false;
			}
			if (n % 2 == 0) {
				genap += n + " ";
			} else {
				ganjil += n + " ";
			}

			n++;
		}
//		System.out.println(ganjil);
//		System.out.println(genap);

		String[] arrayGanjil = ganjil.split(" ");
		String[] arrayGenap = genap.split(" ");

		String output = "";

		for (int i = 0; i < panjangDeret; i++) {
			output += Integer.parseInt(arrayGanjil[i]) + Integer.parseInt(arrayGenap[i]) + " ";
		}
		System.out.println(output);
	}

	private static void soal9() {
		System.out.println("Masukkan N dan T:");
		String nt = input.nextLine().toUpperCase();
		String[] arrayNT = nt.split(" ");

		boolean adaKarakterLain = false;
		int[] banyakNT = new int[2];
		for (int i = 0; i < arrayNT.length; i++) {
			if (arrayNT[i].equals("N")) {
				banyakNT[0]++;
			} else if(arrayNT[i].equals("T")) {
				banyakNT[1]++;
			} else {
				adaKarakterLain = true;
				break;
			}
		}
		
		if(adaKarakterLain) {
			System.out.println("Terdapat karakter lain selain spasi, N dan T");
			return;
		}
		
		int banyakGunungLembah = 0;
		if (banyakNT[0] < banyakNT[1]) {
			banyakGunungLembah = banyakNT[0];
		} else {
			banyakGunungLembah = banyakNT[1];
		}
		System.out.println(banyakNT[0]);
		System.out.println(banyakNT[1]);
		System.out.println("Gunung dan lembah yang sudah dilewati hatori berjumlah: " + banyakGunungLembah / 2);
		System.out.println("Gunung: " + banyakGunungLembah / 4);
		System.out.println("Lembah: " + banyakGunungLembah / 4);

	}

	private static void soal10() {
		// Perhitungan manual mulai------
//		60000 - 11400 = 48600
//		11400 = 60000 - (X - 0.1*X) // cashback
//		X = Y*0.5 // diskon
//
//		11400 = 60000 - ((0.5Y) - 0.1*(0.5Y))
//		11400 = 60000 - (0.5Y - 0.05Y)
//		11400 = 60000 - (0.45Y)
//
//		0.45Y = 48600
//		Y = 108000
//		harga 1 cup = Y / 6
		
		
		// ------
		
		float promoDiskon = 0.5f;
		int maksDiskon = 100000;
		int minOrder = 40000;
		float cashback = 0.1f; // dari harga bayar
		int maksCashback = 30000;

		// System.out.print("Saldo OPO = ");
		int saldoOPO = input.nextInt();//60000;//

		float hargaMinuman = 18000;
		int banyakMinuman = 0;
		float hasil = 0f;
		boolean isLoop = true;
		while (isLoop) {
			banyakMinuman++;
			float hargaAsli = hargaMinuman * banyakMinuman;

			// hitung diskon
			float hargaDiskon = promoDiskon * hargaAsli;
			if (hargaDiskon >= maksDiskon) {
				hargaDiskon = maksDiskon;
			}
			if (hargaAsli < minOrder) {
				hargaDiskon = 0f;
			}

			// hitung cashback
			float hargaCashback = cashback*hargaDiskon;
			if (hargaCashback >= maksCashback) {
				hargaCashback = maksCashback;
			}

			// hitung total bayar
			hasil = hargaDiskon-hargaCashback;

			if (saldoOPO - hargaMinuman < (int) hasil) {
				isLoop = false;
			}
		}

		// bayar bung!!
		saldoOPO -= hasil;
		System.out.println("Jumlah cup =  " + banyakMinuman + "; Saldo akhir = Rp. " + saldoOPO);
	}
}
