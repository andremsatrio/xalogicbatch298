package day4;

import java.util.Scanner;

public class OneDimenNomor4 {

	public static void main(String[] args) {
		System.out.print("Masukkan nilai n: ");
		Scanner input = new Scanner(System.in);

		int n = input.nextInt();
		input.close();

		int[] arrays = new int[n];
		int temp = 1;

		for (int i = 0; i < arrays.length; i++) {
			arrays[i] = temp;
			temp += 4;
			System.out.print(arrays[i] + " ");
		}
	}
}
