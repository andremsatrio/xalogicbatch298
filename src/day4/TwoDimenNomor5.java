package day4;

import java.util.Scanner;

public class TwoDimenNomor5 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.print("Masukkan nilai n: ");
		int n = input.nextInt();

		int[][] arrays = new int[3][n];
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < n; j++) {
				if (i % 3 == 0) {
					arrays[i][j] = j;
				} else if(i%3==1){
					arrays[i][j] = j+n;
				} else {
					arrays[i][j] = j+n+n;
				}
				System.out.print(arrays[i][j]+" ");
			}
			System.out.println();
		}

	}

}
