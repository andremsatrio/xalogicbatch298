package day4;

public class Main {

	public static void main(String[] args) {
		// inisialisasi array
		int[] arrayAngka = new int[5];

		// isi array
		arrayAngka[0] = 1;
		arrayAngka[1] = 2;
		arrayAngka[2] = 3;
		arrayAngka[3] = 4;
		arrayAngka[4] = 5;

		// convert string to int
		arrayAngka[1] = Integer.parseInt("2");

		// akses nilai di suatu array
		int aksesArray = arrayAngka[1];

		for (int i = 0; i < arrayAngka.length; i++) {
			System.out.print(arrayAngka[i] + " ");
		}
		System.out.println();

		System.out.println("Panjang array: " + arrayAngka.length);
		System.out.println(aksesArray);
		
		System.out.println();

		String[] arrayPembelian = new String[5];
		int pembelianPertama = 10000;
		int pembelianKedua = 20000;

		// convert dari int to string cara 1
		arrayPembelian[0] = String.valueOf(pembelianPertama);
		// cara 2
		arrayPembelian[1] = pembelianKedua + "";

		System.out.println("Nilai pembelian: " + arrayPembelian[0]);

	}

}
