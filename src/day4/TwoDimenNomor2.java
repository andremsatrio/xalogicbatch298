package day4;

import java.util.Scanner;

public class TwoDimenNomor2 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.print("Masukkan nilai n: ");
		int n = input.nextInt();
		System.out.print("Masukkan nilai n2: ");
		int n2 = input.nextInt();
		int[][] arrays = new int[n2][n];
		int temp = 0;

		for (int i = 0; i < n2 - 1; i++) {
			for (int j = 0; j < n; j++) {
				if (i % 2 == 0 && j == 0) {
					temp = 0;
				}

				arrays[i][j] = temp;
				System.out.print(arrays[i][j] + " ");

				if (i % 2 == 0) {
					temp = j + 1;
				} else {
					temp *= 3;

					if (j % 3 == 1) {
						temp *= -1;
					} else {
						if (temp < 0) {
							temp *= -1;
						}
					}
				}
			}
			temp = 1;
			System.out.println();
		}
	}
}
