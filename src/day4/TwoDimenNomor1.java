package day4;

import java.util.Scanner;

public class TwoDimenNomor1 {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);
		System.out.print("Masukkan nilai n: ");
		int n = input.nextInt();
		System.out.print("Masukkan nilai n2: ");
		int n2 = input.nextInt();
		int[][] arrays = new int[n2 - 1][n];
		int temp = 0;

		for (int i = 0; i < n2 - 1; i++) {
			for (int j = 0; j < n; j++) {
				if (i % 2 == 0) {
					temp = j;
					arrays[i][j] = temp;
				} else {
					arrays[i][j] = temp;
					temp *= 3;
				}

				System.out.print(arrays[i][j] + " ");
			}
			System.out.println();
			temp = 1;
		}

	}

}
