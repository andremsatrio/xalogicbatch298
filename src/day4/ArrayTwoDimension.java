package day4;

import java.util.Scanner;

public class ArrayTwoDimension {

	public static void main(String[] args) {
		int[][] arrayAngkaDuaDimen = new int[5][5];

		arrayAngkaDuaDimen[2][2] = 13;

		System.out.println("hasil: " + arrayAngkaDuaDimen[2][2]);

		System.out.println();

		// tantangan 1 ----

		// input
		Scanner input = new Scanner(System.in);
		System.out.print("Masukkan nilai n: ");
		int n = input.nextInt();
		int baris = n;
		int kolom = n;

		arrayAngkaDuaDimen = new int[baris][kolom];

		// memory leak
		input.close();

		// proses dan outputnya
		int temp = 0;
		for (int i = 0; i < baris; i++) {
			for (int j = 0; j < kolom; j++) {
				temp++;
				arrayAngkaDuaDimen[i][j] = temp;

				System.out.print(arrayAngkaDuaDimen[i][j] + " ");
			}
			System.out.println();
		}
	}

}
