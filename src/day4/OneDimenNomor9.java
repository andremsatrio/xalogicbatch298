package day4;

import java.util.Scanner;

public class OneDimenNomor9 {
	public static void main(String[] args) {
		System.out.print("Masukkan nilai n: ");
		Scanner input = new Scanner(System.in);

		int n = input.nextInt();
		input.close();

		String[] arrays = new String[n];
		int temp = 4;

		for (int i = 0; i < arrays.length; i++) {
			if (i % 3 != 2) {
				arrays[i] = temp + "";
				temp *= 4;
			} else {
				arrays[i] = "*";
			}
			System.out.print(arrays[i] + " ");
		}
	}
}
