package day4;

import java.util.Scanner;

public class OneDimenNomor10 {
	public static void main(String[] args) {
		System.out.print("masukkan nilai n: ");
		Scanner input = new Scanner(System.in);
		int n = input.nextInt();
		input.close();
		String[] arrays = new String[n];
		int temp = 3;

		for (int i = 0; i < arrays.length; i++) {
			if (i % 4 != 3) {
				arrays[i] = temp + "";
			} else {
				arrays[i] = "XXX";
			}
			temp *= 3;
			System.out.print(arrays[i] + " ");
		}
	}
}
