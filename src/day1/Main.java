package day1;

public class Main {
	public static void main(String[] args) {
		// latihan 1
		int jef = 12;
		System.out.println("jef: "+jef);
		
		// Data type
		int jef1= 12;
		char i = 'i';
		float phi = 3.14f;
		double ipk = 3.7;
		String ganteng = "Achenk";
		boolean benar = true;
		
		
		System.out.println("\n\n");
		
		// contoh kasus
		String nama, alamat;
		int usia;
		double tinggi;
		nama = "Andre";
		alamat = "Jakarta Timur";
		usia = 25;
		tinggi = 172;
		System.out.println("Nama: "+nama);
		System.out.println("Alamat: "+alamat);
		System.out.println("Usia: "+usia+" tahun");
		System.out.println("Tinggi: "+tinggi + " cm");
		
		
		System.out.println("\n\n");
		
		// latihan 2
		// operasi matematika: + - / * % ^ 
		// operasi logic: == != < <= > >= && || 
		// operasi penugasan: = += ++
		int nilaiSatu = 35;
		int nilaiDua = 577;
		System.out.println(nilaiSatu+" + "+nilaiDua+" = "+(nilaiSatu+nilaiDua));
		
		// soal menukar nilai variable tanpa variable penampung (int tmp)
		nilaiSatu += nilaiDua;
		nilaiDua = nilaiSatu-nilaiDua;
		nilaiSatu -= nilaiDua;
		
//		nilaiSatu *= nilaiDua;
//		nilaiDua = nilaiSatu/nilaiDua;
//		nilaiSatu /= nilaiDua;
		
		System.out.println(nilaiSatu+" + "+nilaiDua+" = "+(nilaiSatu+nilaiDua));
	}
}
