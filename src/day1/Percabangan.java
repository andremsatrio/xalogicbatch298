package day1;

import java.util.Scanner;

public class Percabangan {
	public static void main(String[] args) {
		System.out.println("Selamat pagi");
		System.out.println("Selamat datang");

		boolean mauKeluar = false;
		if (mauKeluar) {
			System.out.println("Sampai jumpa");
		} else {
			System.out.println("Tidak jadi keluar");
		}

		System.out.println("\n\n");

		Scanner input = new Scanner(System.in);

//		System.out.print("Masukkan nama siswa: ");
//		String namaSiswa = input.nextLine();
		String namaSiswa = "My Name";

//		input.nextLine(); // untuk perpindahan type data

//		System.out.print("Masukkan batch siswa: ");
//		String namaBatch = input.nextLine(); // untuk input string yang terdapat spasi
		String namaBatch = "Batch 298";

//		System.out.print("Masukkan nilai siswa: ");
//		int nilaiSiswa = input.nextInt();
		int nilaiSiswa = 90;

//		System.out.print("Masukkan nilai Softskill siswa: ");
//		int nilaiSoftskill = input.nextInt(); // max 5
		int nilaiSoftskill = 3;

		String nilaiGrade = "";
		String kelulusan = "";

		if (nilaiSiswa >= 90 && nilaiSoftskill >= 3) {
			nilaiGrade = "A";
			kelulusan = "lulus";
		} else if (nilaiSiswa >= 85 && nilaiSoftskill >= 3) {
			nilaiGrade = "B+";
			kelulusan = "lulus";
		} else if (nilaiSiswa >= 80 && nilaiSoftskill >= 3) {
			nilaiGrade = "B";
			kelulusan = "lulus";
		} else if (nilaiSiswa >= 75 && nilaiSoftskill >= 3) {
			nilaiGrade = "C+";
			kelulusan = "lulus";
		} else if (nilaiSiswa >= 70 && nilaiSoftskill >= 3) {
			nilaiGrade = "C";
			kelulusan = "remedial";
		} else {
			nilaiGrade = "D";
			kelulusan = "tidak lulus";
		}

		System.out.println(namaSiswa + " " + namaBatch + " kelulusan: " + kelulusan + " Grade: " + nilaiGrade);
	}
}
