package day8;

import java.util.Scanner;
import java.lang.Math;

public class Main {

	private static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		String answer = "Y";

		while (answer.toUpperCase().equals("Y")) {
			try {
				System.out.println("Enter the number of case: ");
				int number = input.nextInt();
				input.nextLine();

				switch (number) {
				case 1:
					soal1();
					break;
				case 2:
					soal2();
					break;
				case 3:
					soal3();
					break;
				case 5:
					soal5();
					break;
				case 6:
					soal6();
					break;
				case 7:
					soal7();
					break;
				case 8:
					soal8();
					break;
				case 9:
					soal9();
					break;
				case 10:
					soal10();
					break;
				default:
					System.out.println("Case is not available. Try again!");
				}

			} catch (Exception e) {
				System.out.println(e.getMessage());
			}

			System.out.println();
			System.out.print("Continue? ");
			answer = input.next();
		}

	}

	private static void soal1() {
		System.out.println("Masukkan kalimat: ");
		String string = input.nextLine();
		char[] charString = string.toCharArray();

		int totalKata = 0;
		for (int i = 0; i < charString.length; i++) {
			if (Character.isUpperCase(charString[i]) || i == 0) {
				totalKata++;
			}
		}
		System.out.println(totalKata);
	}

	private static void soal2() {
		// input
		System.out.println("Masukkan panjang password: ");
		int panjangPassword = input.nextInt();
		input.nextLine();
		System.out.println("Masukkan password: ");
		char[] password = input.nextLine().toCharArray();

		// criteria password
		char[] numbers = "0123456789".toCharArray();
		char[] lowerCase = "abcdefghijklmnopqrstuvwxyz".toCharArray();
		char[] upperCase = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();
		char[] specialCharacters = "!@#$%^&*()-+".toCharArray();

		int passwordChecker = 0;

		// logic
		for (int i = 0; i < password.length; i++) {
			// Its length is at least 6
			if (password.length < 6) {
				passwordChecker = 6 - password.length;
				break;
			}
			// It contains at least one digit.
			for (int j = 0; j < numbers.length; j++) {
				if (password[i] != numbers[j]) {
					passwordChecker = 1;
					break;
				}
			}
			if (passwordChecker == 1) {
				break;
			}

			// It contains at least one lower case English character.
			for (int j = 0; j < lowerCase.length; j++) {
				if (password[i] != lowerCase[j]) {
					passwordChecker = 1;
					break;
				}
			}
			if (passwordChecker == 1) {
				break;
			}
			// It contains at least one upper case English character.
			for (int j = 0; j < upperCase.length; j++) {
				if (password[i] != upperCase[j]) {
					passwordChecker = 1;
					break;
				}
			}
			if (passwordChecker == 1) {
				break;
			}
			// It contains at least one special character. The special characters are:
			// !@#$%^&*()-+
			for (int j = 0; j < specialCharacters.length; j++) {
				if (password[i] != specialCharacters[j]) {
					passwordChecker = 1;
					break;
				}
			}
			if (passwordChecker == 1) {
				break;
			}
		}
		// output
		System.out.println(passwordChecker);
	}

	private static void soal3() {
		System.out.println("Masukkan panjang rotasi: ");
		int rotation = input.nextInt();
		input.nextLine();
		System.out.println("Masukkan kalimat: ");
		String string = input.nextLine();
		String lowerString = string.toLowerCase();
		char[] arrayString = string.toCharArray();
		char[] arrayLowerString = lowerString.toCharArray();
		char[] lowerCase = "abcdefghijklmnopqrstuvwxyz".toCharArray();
		char[] rotateLowerCase = new char[lowerCase.length];

		char charTemp = 0;
		for (int i = 0; i < rotation; i++) {
			charTemp = lowerCase[0];
			for (int j = 0; j < lowerCase.length; j++) {
				if (j == lowerCase.length - 1) {// terakhir
					rotateLowerCase[j] = charTemp;
				} else {
					rotateLowerCase[j] = lowerCase[j + 1];
				}
				System.out.print(rotateLowerCase[j]);
			}
			lowerCase = rotateLowerCase;

			System.out.println();
		}
		System.out.println();

		String temp = "";
		String temp1 = "";
		lowerCase = "abcdefghijklmnopqrstuvwxyz".toCharArray();
		for (int i = 0; i < arrayLowerString.length; i++) {
			for (int j = 0; j < lowerCase.length; j++) {
				if (arrayLowerString[i] == lowerCase[j]) {
					temp = rotateLowerCase[j] + "";
				} else if (arrayLowerString[i] == '-') {
					temp = "-";
				}
				if (Character.isUpperCase(arrayString[i])) {
					temp = temp.toUpperCase();
				}
			}
			temp1 += temp;
		}
		System.out.println(temp1);
	}

	private static void soal5() {
		System.out.println("Masukkan total kata: ");
		int totalKata = input.nextInt();
		int n = 0;
		String[] arrayStringHasil = new String[totalKata];

		while (n < totalKata) {
			System.out.println("Masukkan kata: ");
			String inputan = input.next();
			char[] charInputan = inputan.toLowerCase().toCharArray();
			int[] intHackerRank = new int[7];

			int tempInt = 0;

			for (int i = 0; i < charInputan.length; i++) {
				if (charInputan[i] == 'h') {// h
					intHackerRank[0] += 1;
				} else if (charInputan[i] == 'a' && (intHackerRank[0] >= 1 || intHackerRank[5] >= 2)) { // a && (_h ||
																										// r_)
					intHackerRank[1] += 1;
				} else if (charInputan[i] == 'c' && intHackerRank[1] >= 1) { // c && _a
					intHackerRank[2] += 1;
				} else if (charInputan[i] == 'k' && intHackerRank[2] >= 1) { // k && _c
					intHackerRank[3] += 1;
				} else if (charInputan[i] == 'e' && intHackerRank[3] >= 1) { // e && _k
					intHackerRank[4] += 1;
				} else if (charInputan[i] == 'r' && (intHackerRank[4] >= 1 || intHackerRank[5] >= 1)) { // r && (_k ||
																										// _r)
					intHackerRank[5] += 1;
				} else if (charInputan[i] == 'n' && intHackerRank[1] >= 1) { // n && _a
					intHackerRank[6] += 1;
				}
			}

			for (int i = 0; i < intHackerRank.length; i++) {
				if (intHackerRank[i] == 0) {
					tempInt = 0;
					break;
				} else {
					tempInt = 1;
				}

			}

			if (intHackerRank[5] < 2) {
				tempInt = 0;
			}

			if (tempInt == 1) {
				arrayStringHasil[n] = "YES";
			} else {
				arrayStringHasil[n] = "NO";
			}

			n++;
		}

		// output
		for (int i = 0; i < arrayStringHasil.length; i++) {
			System.out.println(arrayStringHasil[i]);
		}
	}

	private static void soal6() {
		System.out.println("Masukkan kalimat: ");
		char[] inputan = input.nextLine().toLowerCase().toCharArray();
		char[] lowerCase = "abcdefghijklmnopqrstuvwxyz".toCharArray();

		int temp = 0;
		for (int i = 0; i < lowerCase.length; i++) {
			for (int j = 0; j < inputan.length; j++) {
				if (inputan[j] == lowerCase[i]) {
					temp++;
					break;
				}
			}
		}

		if (temp == 26) {
			System.out.println("pangram");
		} else {
			System.out.println("not pangram");
		}

	}

	private static void soal7() {

		System.out.println("Masukkan n angka: ");
		int jumlahAngka = input.nextInt();

		input.nextLine();

		System.out.println("Masukkan array angka dengan pemisah \"Enter\"");
		String[] arrayStringAngka = new String[jumlahAngka];
		for (int i = 0; i < jumlahAngka; i++) {
			arrayStringAngka[i] = input.next();
		}

		String tempString = "";
		String[] hasil = new String[arrayStringAngka.length];

		boolean isLoop = true;
		int indeksArrayStringAngka = 0; // indeks
		int digit = 1; // 1 digit angka
		int angkaAwal = 0; // penampung angka awal
		int angka = 0; // penampung angka increment
		boolean isAngkaAwal = true;
		while (isLoop) {
			boolean isMemenuhiKondisi = false;

			String stringAngka = arrayStringAngka[indeksArrayStringAngka];
			char[] arrayCharStringAngka = stringAngka.toCharArray();

			// ambil angka awal jika itu adalah angka awal
			if (digit < arrayCharStringAngka.length && isAngkaAwal) {
				angkaAwal = Integer.parseInt(stringAngka.substring(0, digit));
				angka = Integer.parseInt(stringAngka.substring(0, digit));
				isAngkaAwal = false;
			}

			char[] arrayTempString = tempString.toCharArray();
			if (arrayTempString.length < arrayCharStringAngka.length) {
				// tampung angka
				tempString += angka + "";

				angka++;
			} else if (arrayTempString.length >= arrayCharStringAngka.length) {
//					System.out.println();
				if (tempString.trim().equals(stringAngka)) {
					isMemenuhiKondisi = true;

					hasil[indeksArrayStringAngka] = "YES " + angkaAwal;

					// reset
					digit = 1; // 1 digit angka
					angkaAwal = 0; // penampung angka awal
					angka = 0; // penampung angka increment
					isAngkaAwal = true;
					tempString = "";

				} else {
					isMemenuhiKondisi = false;

					digit += 1; // 1 digit angka

					// reset
					angkaAwal = 0; // penampung angka awal
					angka = 0;
					isAngkaAwal = true;
					tempString = "";

					// kondisi berikut masih kurang tepat
					if (digit > 5 || digit > arrayCharStringAngka.length) {
						hasil[indeksArrayStringAngka] = "NO";
						isMemenuhiKondisi = true;

						// reset
						digit = 1; // 1 digit angka
						angkaAwal = 0; // penampung angka awal
						angka = 0; // penampung angka increment
						isAngkaAwal = true;
						tempString = "";
					}
				}
			} else if (arrayCharStringAngka.length == 1) {
				hasil[indeksArrayStringAngka] = "NO";
				isMemenuhiKondisi = true;

				// reset
				digit = 1; // 1 digit angka
				angkaAwal = 0; // penampung angka awal
				angka = 0; // penampung angka increment
				isAngkaAwal = true;
				tempString = "";
			}

//			System.out.println(tempString+" " + "digit: "+digit);
//			System.out.println(hasil[indeksArrayStringAngka] +" ");

			// akan melihat angka berikutnya jika memenuhi kondisi
			if (isMemenuhiKondisi) {
//				System.out.println(hasil[indeksArrayStringAngka] +" ");
				indeksArrayStringAngka++;
			}

			// stop iterasi jika indeksArrayStringAngka melebihi panjang array
			// arrayStringAngka
			if (indeksArrayStringAngka > arrayStringAngka.length - 1) {
				isLoop = false;
			}

//			System.out.println();
		}

		// output
		for (int i = 0; i < hasil.length; i++) {
			System.out.println(hasil[i]);
		}
	}

	private static void soal8() {
		System.out.println("Masukkan jumlah kata: ");
		int jumlahKata = input.nextInt();
		input.nextLine();

		System.out.println("Masukkan kata:");
		String inputan = "";
		for (int i = 0; i < jumlahKata; i++) {
			if (i < jumlahKata - 1) {
				inputan += input.next() + " ";
			} else {
				inputan += input.next();
			}
		}
		inputan = inputan.toLowerCase();
		String[] arrayInputan = inputan.split(" ");
		char[] charArrayInputan = inputan.toCharArray();

		// !"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstuvwxyz{|}~
		String tempForCharacter = "";
		for (int i = 33; i < 127; i++) {
			tempForCharacter += (char) i;
		}
		char[] characters = tempForCharacter.toCharArray();
		int totalCharacter = characters.length;
//		System.out.println(totalCharacter);

		int[] intInputan = new int[characters.length * arrayInputan.length];// 26*3=78

		int hasil = 0;

		int spasi = 0;
		for (int i = 0; i < charArrayInputan.length; i++) {
			char charTemp = charArrayInputan[i];
			if (charTemp == ' ') {
				spasi++;
			}

			for (int j = 0; j < characters.length; j++) {
				if (charTemp == characters[j]) {
					intInputan[j + (characters.length * spasi)] += 1;
				}
			}

			// System.out.print(charArrayInputan[i]);
		}
		// System.out.println();

		spasi = 0;
		int[] sumIntInputan = new int[characters.length];
		for (int i = 0; i < intInputan.length; i++) {

			int iPlusSatu = i + 1;

			// System.out.print(intInputan[i]+" ");
			if (iPlusSatu % totalCharacter == 0 && i != 0) {
				// System.out.println(" "+i);
				spasi++;
			}

		}

		// System.out.println();

		sumIntInputan = new int[characters.length];
		for (int i = 0; i < intInputan.length; i++) {
			if (intInputan[i] > 0) {
				sumIntInputan[i % totalCharacter] += 1;
			} else {
				sumIntInputan[i % totalCharacter] = 0;
			}

			// System.out.print(sumIntInputan[i%totalCharacter]+" ");

		}

		for (int i = 0; i < sumIntInputan.length; i++) {
			if (sumIntInputan[i] > 2) {
				hasil++;
			}
		}

		System.out.print(hasil);

		System.out.println();
	}

	private static void soal9() {
		System.out.println("Masukkan 2 kata");
		String input1 = input.next();
		String input2 = input.next();

		char[] charInput1 = input1.toCharArray();
		char[] charInput2 = input2.toCharArray();

		int temp = 0;
		for (int i = 0; i < charInput1.length; i++) {
			for (int j = 0; j < charInput2.length; j++) {
				if (charInput1[i] == charInput2[j]) {
					temp++;
				}
			}
		}

		temp = (charInput1.length + charInput2.length) - (temp * 2);

		System.out.println(temp);
	}

	private static void soal10() {
		// input
		System.out.println("Masukkan jumlah n: ");
		int jumlahIterasi = input.nextInt();
		input.nextLine();

		int n = 0;
		String[] hasil = new String[jumlahIterasi];
		while (n < jumlahIterasi) {
			System.out.println("Masukkan 2 kata (enter): ");
			String string1 = input.next();
			String string2 = input.next();

			String vokalKonsonan = string1 + " " + string2;

			char charTemp = 0;

			// memisahkan vokal
			int[] intVokal = new int[26 * 2];
			char[] charVokal = vokalKonsonan.toLowerCase().toCharArray();
			int spasi = 0;
			for (int i = 0; i < charVokal.length; i++) {
				charTemp = charVokal[i];
				if (charTemp == ' ') {
					spasi = 1;
				}

				if (charTemp == 'a') {
					intVokal[0 + (26 * spasi)] += 1;
				} else if (charTemp == 'b') {
					intVokal[1 + (26 * spasi)] += 1;
				} else if (charTemp == 'c') {
					intVokal[2 + (26 * spasi)] += 1;
				} else if (charTemp == 'd') {
					intVokal[3 + (26 * spasi)] += 1;
				} else if (charTemp == 'e') {
					intVokal[4 + (26 * spasi)] += 1;
				}

				else if (charTemp == 'f') {
					intVokal[5 + (26 * spasi)] += 1;
				} else if (charTemp == 'g') {
					intVokal[6 + (26 * spasi)] += 1;
				} else if (charTemp == 'h') {
					intVokal[7 + (26 * spasi)] += 1;
				} else if (charTemp == 'i') {
					intVokal[8 + (26 * spasi)] += 1;
				}

				else if (charTemp == 'j') {
					intVokal[9 + (26 * spasi)] += 1;
				} else if (charTemp == 'k') {
					intVokal[10 + (26 * spasi)] += 1;
				} else if (charTemp == 'l') {
					intVokal[11 + (26 * spasi)] += 1;
				} else if (charTemp == 'm') {
					intVokal[12 + (26 * spasi)] += 1;
				}

				else if (charTemp == 'n') {
					intVokal[13 + (26 * spasi)] += 1;
				} else if (charTemp == 'o') {
					intVokal[14 + (26 * spasi)] += 1;
				} else if (charTemp == 'p') {
					intVokal[15 + (26 * spasi)] += 1;
				} else if (charTemp == 'q') {
					intVokal[16 + (26 * spasi)] += 1;
				}

				else if (charTemp == 'r') {
					intVokal[17 + (26 * spasi)] += 1;
				} else if (charTemp == 's') {
					intVokal[18 + (26 * spasi)] += 1;
				} else if (charTemp == 't') {
					intVokal[19 + (26 * spasi)] += 1;
				} else if (charTemp == 'u') {
					intVokal[20 + (26 * spasi)] += 1;
				}

				else if (charTemp == 'v') {
					intVokal[21 + (26 * spasi)] += 1;
				} else if (charTemp == 'w') {
					intVokal[22 + (26 * spasi)] += 1;
				} else if (charTemp == 'x') {
					intVokal[23 + (26 * spasi)] += 1;
				} else if (charTemp == 'y') {
					intVokal[24 + (26 * spasi)] += 1;
				} else if (charTemp == 'z') {
					intVokal[25 + (26 * spasi)] += 1;
				}

			}

			hasil[n] = "NO";
			for (int i = 0; i < (intVokal.length / 2); i++) {
				if (intVokal[i] > 0 && intVokal[i] == intVokal[i + (intVokal.length / 2)]) {
					hasil[n] = "YES";
					break;
				}

			}

			n++;
		}

		// output
		for (int i = 0; i < hasil.length; i++) {
			System.out.println(hasil[i]);
		}

	}
}
