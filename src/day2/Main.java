package day2;

import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		System.out.println("Input Hadiah (Pilih Nomor 1-5): ");
		int inputHadiah = input.nextInt();

		switch (inputHadiah) {
		case 1:
			System.out.println("Selamat Anda mendapatkan mobil");
			break;
		case 2:
			System.out.println("Selamat Anda mendapatkan tiket pulang");
			break;
		case 3:
			System.out.println("Selamat Anda mendapatkan boneka beruang");
			break;
		case 4:
			System.out.println("Selamat Anda mendapatkan liburan ke india");
			break;
		case 5:
			System.out.println("Selamat Anda mendapatkan motor harley");
			break;
		default:
			System.out.println("Nomor yang Anda pilih tidak sesuai");
			break;
		}

	}
}
