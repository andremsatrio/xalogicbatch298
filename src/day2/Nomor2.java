package day2;

import java.util.Scanner;

/**
 * Kasus GrabFood
 */
public class Nomor2 {
	public static void main(String[] args) {
		// input
		Scanner input = new Scanner(System.in);

		System.out.println("Masukkan nominal belanja Anda: ");
		int belanja = input.nextInt();
		System.out.println("Masukkan jarak tempuh : ");
		int jarak = input.nextInt();
		System.out.println("Masukkan kode promo : ");
		String promo = input.next();

		int ongkir = 0;
		int getDiskon = 0;

		// logic promo dan diskon
		if (promo.equals("JKTOVO") && belanja >= 30000) {
			getDiskon = (int) (0.4f * belanja);
			if (getDiskon > 30000) {
				getDiskon = 30000;
			}
		}

		// logic ongkir
		if (jarak <= 5) {
			ongkir = 5000;
		} else {
			ongkir = 5000 + ((jarak - 5) * 1000);
		}

		// output
		System.out.println("Belanja : " + belanja);
		System.out.println("Diskon 40% : " + getDiskon);
		System.out.println("Ongkir : " + ongkir);
		System.out.println("Total Belanja : " + (belanja - getDiskon + ongkir));
	}
}
