package day2;

import java.util.Scanner;

/**
 * Kasus: Pulsa dan poin yang didapatkan
 */
public class Nomor1 {
	public static void main(String[] args) {
		// input
		Scanner input = new Scanner(System.in);

		System.out.println("Masukkan nominal Pulsa yang akan Anda beli: ");
		int nominalPulsa = input.nextInt();

		int getPoint;

		// logic mendapatkan poin
		if (nominalPulsa >= 100000) {
			getPoint = 800;
		} else if (nominalPulsa >= 50000) {
			getPoint = 400;
		} else if (nominalPulsa >= 25000) {
			getPoint = 200;
		} else if (nominalPulsa >= 10000) {
			getPoint = 80;
		} else {
			getPoint = 0;
		}

		// output
		System.out.println("Pulsa: " + nominalPulsa);
		System.out.println("Point: " + getPoint);

	}
}
