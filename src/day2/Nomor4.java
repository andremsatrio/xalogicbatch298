package day2;

import java.util.Scanner;

/**
 * Kasus: Pulsa dan poin versi 2
 */
public class Nomor4 {
	public static void main(String[] args) {
		// input
		Scanner input = new Scanner(System.in);

		System.out.println("Masukkan nominal Pulsa yang akan Anda beli : ");
		int pulsa = input.nextInt();
//		int pulsa = 20000;
		int poinSatu = 0;
		int poinDua = 0;
		int poinTiga = 0;

		// logic mendapatkan poin berdasarka pulsa dan outputnya
		if (pulsa > 30000) {
			poinSatu = 0;
			poinDua = (pulsa - 10000) / 1000;
			if (poinDua > 20) {
				poinDua = 20;
			}
			poinTiga = 2 * ((pulsa - 30000) / 1000);

			System.out.println(poinSatu + " + " + poinDua + " + " + poinTiga + " = " + (poinSatu + poinDua + poinTiga));
		} else if (pulsa > 10000) {
			poinSatu = 0;
			poinDua = (pulsa - 10000) / 1000;

			System.out.println(poinSatu + " + " + poinDua + " = " + (poinSatu + poinDua));
		} else {
			poinSatu = 0;

			System.out.println(poinSatu + " = " + poinSatu);
		}
	}
}
