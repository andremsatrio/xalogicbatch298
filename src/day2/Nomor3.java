package day2;

import java.util.Scanner;

/**
 * Kasus: Sopi
 */
public class Nomor3 {
	public static void main(String[] args) {
		// input
		Scanner input = new Scanner(System.in);

		System.out.println("Masukkan nominal belanja Anda : ");
		int belanja = input.nextInt();
		System.out.println("Masukkan nominal ongkos kirim Anda : ");
		int ongkir = input.nextInt();

		int diskonOngkir;
		int potonganHarga;

		// logic ongkir dan potongan harga
		if (belanja >= 100000) {
			diskonOngkir = 10000;
			potonganHarga = 20000;
		} else if (belanja >= 50000) {
			diskonOngkir = 10000;
			potonganHarga = 10000;
		} else if (belanja >= 30000) {
			diskonOngkir = 5000;
			potonganHarga = 5000;
		} else {
			diskonOngkir = 0;
			potonganHarga = 0;
		}

		// output
		System.out.println("Belanja : " + belanja);
		System.out.println("Ongkos Kirim : " + ongkir);
		System.out.println("Diskon Ongkir : " + diskonOngkir);
		System.out.println("Diskon Belanja : " + potonganHarga);
		System.out.println("Total Belanja : " + (belanja + ongkir - diskonOngkir - potonganHarga));
	}
}
