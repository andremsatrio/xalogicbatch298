package day6;

import java.util.Scanner;

public class Main {

	private static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {

//		String answer = "Y";
//
//		while (answer.toUpperCase().equals("Y")) {
//			try {
//				System.out.println("Enter the number of case: ");
//				int number = input.nextInt();
//				input.nextLine();
//
//				switch (number) {
//				case 1:
//					soal1();
//					break;
//				case 2:
//					soal2();
//					break;
//				case 3:
//		soal3();
//					break;
//				case 4:
//		soal4();
//					break;
//				case 5:
//					soal5();
//					break;
//				case 6:
//					soal6();
//					break;
//				case 7:
		soal7();
//					break;
//				default:
//					System.out.println("Case is not available. Try again!");
//				}
//
//			} catch (Exception e) {
//				System.out.println(e.getMessage());
//			}
//
//			System.out.println();
//			System.out.print("Continue? ");
//			answer = input.next();
//		}
	}

	private static void soal1() {
		System.out.println("soal 1 ============");
		System.out.print("Masukkan panjang nilai bilangan Fibonaci: ");
		int inputFibonaci = input.nextInt();
		int[] arrayFibonaci = new int[inputFibonaci];
		String temp = "";
		for (int i = 0; i < inputFibonaci; i++) {// 0 1

			// fibonaci
			if (i > 1) {
				arrayFibonaci[i] = arrayFibonaci[i - 1] + arrayFibonaci[i - 2];// mulai dari index ke 2
			} else {
				arrayFibonaci[i] = 1;// mulai dari index ke 0 sampai 1
			}

			temp += arrayFibonaci[i];// 1

			// tambahkan koma
			if (i < inputFibonaci - 1) {
				temp += ",";// 1,
			}
		}

		System.out.println(temp);
	}

	private static void soal2() {
		System.out.println("soal 2 ================");
		System.out.print("Masukkan banyak bilangan: ");
		int inputan = input.nextInt();
		int[] arrayInputan = new int[inputan];
		String temp = "";

		for (int i = 0; i < inputan; i++) {
			if (i > 2) {
				arrayInputan[i] = arrayInputan[i - 3] + arrayInputan[i - 2] + arrayInputan[i - 1];
			} else {
				arrayInputan[i] = 1;
			}

			temp += arrayInputan[i];
			if (i < inputan - 1) {
				temp += ",";
			}
		}

		System.out.println(temp);
	}

	private static void soal3() {
		System.out.println("Soal 3 ==============");
		System.out.println("Masukkan bilangan prima(max kelipatan 97): ");
		int prima = input.nextInt();

		String tempString = "";
		for (int i = 2; i < prima; i++) {
			boolean isPrima = true;
			for (int j = 2; j < i; j++) {
				if (i % j == 0) {
					isPrima = false;
					break;
				}
			}
			if (isPrima) {
				tempString += i + ",";
			}
		}

		// hilangkan koma
		String[] charPrima = tempString.split(",");
		tempString = "";
		for (int i = 0; i < charPrima.length; i++) {
			if (i < charPrima.length - 1) {
				tempString += charPrima[i] + ",";
			} else {
				tempString += charPrima[i];
			}
		}

		System.out.print(tempString);
	}

	private static void soal4() {
		System.out.println("Soal 4 ============");
		System.out.println("Masukkan waktu: ");
		String waktu = input.next();

		String jamMenitDetikSaja = waktu.substring(0, 8);
		String jamSaja = jamMenitDetikSaja.substring(0, 2);
		String menitDetikSaja = jamMenitDetikSaja.substring(2, 8);
		String amPm = waktu.substring(8, 10);

		String temp = "";

		if ((12 + Integer.parseInt(jamSaja)) >= 12) {
			temp = jamMenitDetikSaja;
			if (amPm.toUpperCase().equals("PM")) {
				temp = (12 - Integer.parseInt(jamSaja)) + menitDetikSaja;
			}
		} else {

			if (amPm.toUpperCase().equals("PM")) {
				temp = (12 + Integer.parseInt(jamSaja)) + menitDetikSaja;
			} else if (amPm.toUpperCase().equals("AM")) {
				temp = jamMenitDetikSaja;
			}
		}

		System.out.println(temp);
	}

	private static void soal5() {
		System.out.println("Soal 5 ======");
		System.out.println("Masukkan nilai untuk difaktorisasi (max kelipatan 97): ");
		int poonPaktor = input.nextInt();

		String temp = "";

		int[] arrayPrima = { 2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89,
				97, 2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97 };

		boolean isLoop = true;
		while (isLoop) {
			for (int i = 0; i < arrayPrima.length; i++) {
				int pembagi = arrayPrima[i];
				if (poonPaktor % pembagi == 0) {
					temp += poonPaktor + " / " + pembagi + " = " + (poonPaktor / pembagi) + "\n";
					poonPaktor = poonPaktor / pembagi;
					break;
				} else if (i == arrayPrima.length - 1 && poonPaktor % pembagi != 0) {
					if (temp.isEmpty() || temp.equals("")) {
						temp = "Mohon maaf, nilai yang anda masukkan tidak dapat kami faktorisasi";
					}
					isLoop = false;
				}
			}
		}

		System.out.println(temp);
	}

	private static void soal6() {
		System.out.println("soal 6 ==============");
		System.out.print("Masukkan indeks kostemer: ");
		String inputKostemer = input.nextLine();
		int[] arrayJarakKostemer = { 2000, 500, 1500, 300 }; // jarak dalam Meter

		int bensin = 2500; // bensin 1 liter untuk jarak 2.5KM

		String tempString = "";
		int tempInt = 0;

		String[] arrayInputKostemer = inputKostemer.split(" ");

		// pengaman jika panjang input array melebihi
		// panjang jarak kostemer
		if (arrayInputKostemer.length > arrayJarakKostemer.length) {
			System.out.println("Input melebihi database");
			return;
		}

		// pengaman jika tidak ada inputan 1 (toko)
		if (!arrayInputKostemer[0].equals("1")) {
			System.out.println("Maaf, barang orderan ketinggalan ditoko.\nSilahkan input ulang!!");
			return;
		}

		// pengaman jika indeks maksimum inputan besar
		for (int i = 0; i < arrayInputKostemer.length; i++) {
			if (Integer.parseInt(arrayInputKostemer[i]) > tempInt) {
				tempInt = Integer.parseInt(arrayInputKostemer[i]);
			}
		}
		if (tempInt > arrayJarakKostemer.length) {
			System.out.println("Indeks maksimal input melebihi database");
			return;
		}

		// pengaman jika inputan longkap
		for (int i = 0; i < tempInt; i++) {
			tempString += (i + 1) + "";
			if (i != tempInt - 1) { // tambah spasi
				tempString += " ";
			}
		}
		arrayInputKostemer = tempString.split(" ");
		tempString = "";
		tempInt = 0;

		// perhitungan jarak
		for (int i = 0; i < arrayInputKostemer.length; i++) {
			int kostemer = Integer.parseInt(arrayInputKostemer[i]) - 1;

			String jarakKostemer = "";
			if (arrayJarakKostemer[kostemer] <= 1000) {
				jarakKostemer = arrayJarakKostemer[kostemer] + "M";
			} else {
				jarakKostemer = (arrayJarakKostemer[kostemer] / 1000.0) + "KM";
			}

			if (i == 0) {
				tempString += "Jarak tempuh = " + jarakKostemer + " + ";
				tempInt += arrayJarakKostemer[kostemer];
			} else if (i == arrayInputKostemer.length - 1) {
				tempString += jarakKostemer;
				tempInt += arrayJarakKostemer[kostemer];
			} else {
				tempString += jarakKostemer + " + ";
				tempInt += arrayJarakKostemer[kostemer];
			}
		}
		// perhitungan hasil jarak
		if (tempInt <= 1000) {
			tempString += " = " + tempInt + "M" + "\n";
		} else {
			tempString += " = " + (tempInt / 1000.0) + "KM" + "\n";
		}

		// perhitungan bensin
		tempInt /= bensin;
		if (tempInt % bensin != 0) { // jika bensin terdapat sisa maka ditambah 1 liter
			tempInt++;
		}
		tempString += "Bensin = " + tempInt + " Liter";

		// hapus semua .0
		tempString = tempString.replaceAll(".0K", "K");

		System.out.println(tempString);
	}

	private static void soal7() {
		System.out.println("soal 7 ==============");
		System.out.print("Masukkan SOS Anda: ");
		String sos = input.nextLine();
		char[] arrayCharSos = sos.toLowerCase().toCharArray();
		int temp = 0;

		// pengaman jika panjang inputan tidak sesuai 3 digit // sossop
		if (arrayCharSos.length % 3 > 0) {
			if (arrayCharSos.length % 3 == 1) {// jika 1 tidak 3 digit
				sos += "pp";
				arrayCharSos = sos.toLowerCase().toCharArray();
			} else if (arrayCharSos.length % 3 == 2) { // 2 digit
				sos += "p";
				arrayCharSos = sos.toLowerCase().toCharArray();
			}
		}

		// hitung error nya
//		for (int i = 0; i < arrayCharSos.length; i++) {//sossop
//			if (i % 3 == 0 && arrayCharSos[i] == 's') {//s
//				continue;
//			} else if (i % 3 == 1 && arrayCharSos[i] == 'o') {//o
//				continue;
//			} else if (i % 3 == 2 && arrayCharSos[i] == 's') {//s
//				continue;
//			} else { // 
//				temp++;
//			}
//			
//			
//			System.out.println();
//		}

		// cara ka Arya
		for (int i = 0; i < arrayCharSos.length; i += 3) {// P Q Q
			if (arrayCharSos[i] != 's') {
				temp++;
			}
			if (arrayCharSos[i + 1] != 'o') {
				temp++;
			}
			if (arrayCharSos[i + 2] != 's') {
				temp++;
			}
		}

		System.out.println(temp);
	}

}
