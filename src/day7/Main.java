package day7;

import java.util.Scanner;
import java.text.DecimalFormat;
import java.math.BigDecimal;

public class Main {
	
	private static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		String answer = "Y";

		while (answer.toUpperCase().equals("Y")) {
			try {
				System.out.println("Enter the number of case: ");
				int number = input.nextInt();
				input.nextLine();

				switch (number) {
				case 3:
					soal3();
					break;
				case 4:
					soal4();
					break;
				case 5:
					soal5();
					break;
				case 6:
					soal6();
					break;
				case 7:
					soal7();
					break;
				case 8:
					soal8();
					break;
				case 9:
					soal9();
					break;
				case 10:
					soal10();
					break;
				default:
					System.out.println("Case is not available. Try again!");
				}

			} catch (Exception e) {
				System.out.println(e.getMessage());
			}

			System.out.println();
			System.out.print("Continue? ");
			answer = input.next();
		}

	}

	private static void soal3() {
		System.out.println("Soal 3 ========");

		// input
		System.out.print("Masukkan panjang array: ");
		int panjangArray = input.nextInt();
		input.nextLine();
		System.out.print("Masukkan array: ");
		String simpleArray = input.nextLine();

		String[] arrayStringSimpleArray = simpleArray.split(" ");

		// pengaman panjang array
		if (panjangArray != arrayStringSimpleArray.length) {
			System.out.println("panjang array tidak sesuai");
			return;
		}

		int temp = 0;
		for (int i = 0; i < arrayStringSimpleArray.length; i++) {
			temp += Integer.parseInt(arrayStringSimpleArray[i]);
		}

		// output
		System.out.println(temp);
	}

	private static void soal4() {
		// Given a square matrix,
		// calculate the absolute difference between
		// the sums of its diagonals

		System.out.println("soal 4 =======");

		// input
		System.out.print("Masukkan n matrix persegi: ");
		int n = input.nextInt();
		input.nextLine();
		System.out.println("Masukkan array matrix persegi (" + n + "x" + n + ") ");
		String matriks = "";
		for (int i = 0; i < n; i++) {
			if (i == n - 1) {
				matriks += input.nextLine();
			} else {
				matriks += input.nextLine();
				matriks += " ";
			}
		}

		String[] arrayMatriks = matriks.split(" ");

		// pengaman jika panjang array tidak sesuai
		if (arrayMatriks.length % n != 0) {
			System.out.println("Panjang array dan n tidak sesuai");
			return;
		}

		int sumLeftToRight = 0;
		int sumRightToLeft = 0;

		int baris = (int) Math.sqrt(arrayMatriks.length);
		int kolom = (int) Math.sqrt(arrayMatriks.length);
		int indeksMatriks = 0;

		for (int i = 0; i < baris; i++) {
			for (int j = 0; j < kolom; j++) {
				if (i == j) {
					sumLeftToRight += Integer.parseInt(arrayMatriks[indeksMatriks]);
				}
				if (i + j == baris - 1) {
					sumRightToLeft += Integer.parseInt(arrayMatriks[indeksMatriks]);
				}

				indeksMatriks++;
			}

		}

		// output
		System.out.println(Math.abs(sumLeftToRight - sumRightToLeft));
	}

	private static void soal5() {
		// input
		System.out.print("Masukkan n: ");
		int n = input.nextInt();
		input.nextLine();
		System.out.print("Masukkan array: ");
		String string = input.nextLine();

		String[] arrayString = string.split(" ");

		// pengaman panjang array
		if (n != arrayString.length) {
			System.out.println("Panjang array tidak sesuai. Coba lagi!");
			return;
		}

		int negative = 0;
		int zero = 0;
		int positive = 0;

		for (int i = 0; i < arrayString.length; i++) {
			int value = Integer.parseInt(arrayString[i]);
			if (value < 0) {
				negative++;
			} else if (value == 0) {
				zero++;
			} else {
				positive++;
			}
		}

		// reformat output menjadi 6 angka dibelakang koma
		DecimalFormat decimalFormat = new DecimalFormat("0.000000");
		string = decimalFormat.format((float) (positive) / n) + "\n" + decimalFormat.format((float) (negative) / n)
				+ "\n" + decimalFormat.format((float) (zero) / n);

		// output
		System.out.println(string);
	}

	private static void soal6() {
		// input
		System.out.println("Masukkan n: ");
		int n = input.nextInt();

		// pola segitiga siku-siku
		String temp = "";
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				if (i + j >= n - 1) {
					temp += "#";
				} else {
					temp += "   ";
				}
			}
			temp += "\n";
		}

		// output
		System.out.println(temp);
	}

	private static void soal7() {
		// input
		System.out.println("Masukkan array: ");
		String string = input.nextLine();

		// ubah string ke array berdasarkan spasi
		String[] arrayString = string.split(" ");

		// inisialisasi nilai awal max dan min
		int max = Integer.MIN_VALUE;
		int min = Integer.MAX_VALUE;
		for (int i = 0; i < arrayString.length; i++) {
			int sum = 0;
			for (int j = 0; j < arrayString.length; j++) {
				if (i != j) {
					sum += Integer.parseInt(arrayString[j]);
				}
			}

			// logic menyimpan nilai tertinggi
			if (max < sum) {
				max = sum;
			}

			// logic menyimpan nilai terendah
			if (min > sum) {
				min = sum;
			}
		}

		System.out.println(max + " " + min);

	}

	private static void soal8() {
		System.out.println("masukkan n: ");
		int n = input.nextInt();
		input.nextLine();
		System.out.println("Masukkan array: ");
		String string = input.nextLine();

		String[] arrayString = string.split(" ");

		// pengaman panjang array jika tidak sesuai
		if (n != arrayString.length) {
			System.out.println("Panjang array tidak sesuai");
			return;
		}

		// cari tertinggi
		int max = -99999999;
		for (int i = 0; i < arrayString.length; i++) {
			if (max < Integer.parseInt(arrayString[i])) {
				max = Integer.parseInt(arrayString[i]);
			}
		}

		int nMax = 0;
		for (int i = 0; i < arrayString.length; i++) {
			if (max == Integer.parseInt(arrayString[i])) {
				nMax++;
			}
		}

		System.out.println(nMax);
	}

	private static void soal9() {
		// input
		System.out.println("Masukkan nilai n: ");
		int n = input.nextInt();
		input.nextLine();
		System.out.println("Masukkan array: ");
		String string = input.nextLine();

		// konvert string ke array berdasarkan regex spasi
		String[] arrayString = string.split(" ");

		// pengaman jika panjang array tidak sesuai
		if (n != arrayString.length) {
			System.out.println("Panjang array tidak sesuai, silahkan dicoba lagi");
			return;
		}

		// penjumlahan bilangan besar
		BigDecimal temp = new BigDecimal(0);
		for (int i = 0; i < arrayString.length; i++) {
			temp = temp.add(new BigDecimal(arrayString[i]));

		}

		// output
		System.out.println(temp);

	}

	private static void soal10() {
		System.out.println("Masukkan array (2x3): ");
		String string1 = input.nextLine();
		String string2 = input.nextLine();

		String[] arrayString1 = string1.split(" ");
		String[] arrayString2 = string2.split(" ");

		// pengaman jika panjang array tidak sama
		if (arrayString1.length != arrayString2.length) {
			System.out.println("Panjang array tidak sama, mohon diinput ulang!");
			return;
		}

		int pointAlice = 0;
		int pointBob = 0;

		for (int i = 0; i < arrayString1.length; i++) {
			int alice = 0;
			for (int j = 0; j < arrayString2.length; j++) {
				int bob = 0;

				if (i == j) {
					alice = Integer.parseInt(arrayString1[i]);
					bob = Integer.parseInt(arrayString2[j]);
					if (alice > bob) {
						pointAlice++;
					} else if (alice < bob) {
						pointBob++;
					}
				}

			}
		}

		System.out.println(pointAlice + " " + pointBob);
	}
}
