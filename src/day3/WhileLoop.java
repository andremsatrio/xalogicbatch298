package day3;

import java.util.Scanner;

public class WhileLoop {

	public static void main(String[] args) {
		// input
		Scanner input = new Scanner(System.in);

		System.out.print("Masukkan nilai 5: ");
		int intLoop = input.nextInt();

		int i = 0;

		// increment
		while (i < intLoop) {
			i++;
			System.out.println(i);
		}

		System.out.println();

		// decrement
		while (i > 0) {
			System.out.println(i);
			i--;
		}

		// close input (memory leak)
		input.close();

	}
}
