package day3;

import java.util.Scanner;

public class DoWhile {

	public static void main(String[] args) {
		// input
		Scanner input = new Scanner(System.in);

		System.out.print("Masukkan nilai 5: ");
		int n = input.nextInt();

		int i = 0;
		do {
			if(i==n-1) {
				System.out.print(i+1);
			} else {
				System.out.print((i+1) + ",");
			}

			i++;
		} while (i < 5);
		
		// close input (memory leak)
		input.close();
	}

}
