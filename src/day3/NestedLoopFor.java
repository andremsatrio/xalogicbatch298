package day3;

import java.util.Scanner;

public class NestedLoopFor {

	public static void main(String[] args) {
		//
		Scanner input = new Scanner(System.in);

//		System.out.print("Masukkan nilai 5 : ");
//		int n = input.nextInt();
		int n = 5;

		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {	
				
				// pola segitiga siku-siku
				//vvvvx
				//vvvxx
				//vvxxx
				//vxxxx
				//xxxxx
//				if (i + j >= n-1) {
//					System.out.print("x");
//				} else {
//					System.out.print("v");
//				}
				
				// pola X
				//x   x
				// x x 
				//  x  
				// x x 
				//x   x
//				if(i==j || i+j==n-1) {
//					System.out.print("x");
//				} else {
//					System.out.print(" ");
//				}
				
				// pola segitiga samping kiri
				//x
				//xx
				//xxx
				//xx  
				//x
				if(i>=j) {
					if(i+j <= n-1) {
						System.out.print("x");
					} else {
						System.out.print(" ");
					}
				}
			}
			System.out.println();
		}
	}
}
